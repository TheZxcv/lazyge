# Lazy Gaussian Elimination [![pipeline status](https://gitlab.com/TheZxcv/lazyge/badges/master/pipeline.svg)](https://gitlab.com/TheZxcv/lazyge/-/commits/master)[![coverage report](https://gitlab.com/TheZxcv/lazyge/badges/master/coverage.svg)](https://gitlab.com/TheZxcv/lazyge/-/commits/master)

## Build

```bash
$ mkdir build
$ cd build
$ cmake -DCMAKE_BUILD_TYPE=Release ..
$ make
```
## Usage

```bash
$ lge [equation-system-file]
```

## Example input
```
c this is a comment
c modulo #variables #equations
h 2      5          4
c v0 v1 v2 v3 v4 b
e  1  1  0  1  0 0
e  1  1  0  0  0 0
e  0  1  1  0  1 1
e  0  0  1  1  1 1
```
