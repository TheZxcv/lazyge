#!/bin/sh
clang-format --style=llvm -i inc/*.h src/*.cpp test/*.cpp
g++ -o ge inc/*.h src/*.cpp -I inc/
doxygen config > /dev/null
