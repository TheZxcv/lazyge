#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import numpy as np


def main():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-n', '--modulo', type=int, default=256, help='Modulo')
    parser.add_argument('-e', '--equations', type=int, default=10000,
                        help='Number of equations')
    parser.add_argument('-v', '--variables', type=int, default=100,
                        help='Number of variables')
    parser.add_argument('-d', '--density', type=float, default=0.05,
                        help='Density')
    parser.add_argument('output', help='Output file')
    args = parser.parse_args()

    modulo = args.modulo
    nequations = args.equations
    nvars = args.variables
    density = args.density

    mat_mask = np.random.binomial(
        1, density, size=nequations*(nvars+1))
    nonzeros = sum(mat_mask)
    zeros = len(mat_mask) - nonzeros
    mat_mask = mat_mask.reshape(nequations, nvars+1)

    ran_mat = np.random.rand(nequations, nvars + 1) * mat_mask
    mat = (modulo * ran_mat).astype(int)

    with open(args.output, 'w') as output:
        output.write("c density {}\n".format(nonzeros / (zeros + nonzeros)))

        output.write("c modulo #vars #equations\n")
        output.write("h {modulo} {nvars} {nequations}\n".format(
            modulo=modulo, nvars=nvars, nequations=nequations))
        for line in mat:
            output.write("e {coeffs}\n".format(
                coeffs=' '.join(str(e) for e in line)))


if __name__ == '__main__':
    main()
