#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd
import subprocess
import sys

from os import cpu_count, path
from tqdm import tqdm
from concurrent.futures import ThreadPoolExecutor as Pool, wait, FIRST_COMPLETED


def main(dataset, destination):
    df = pd.read_csv(dataset, ',')

    pool = Pool(max_workers=cpu_count())
    futures = []
    for irow in range(len(df)):
        row = df.iloc[irow]
        args = ['./lge', '-D', '-r',
                row.rnd_options, '-d', row.filename]
        if row['is sparse']:
            args.append('-S')
        future = pool.submit(subprocess.call, args, encoding='utf-8',
                             stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        futures.append(future)

    with tqdm(total=len(futures)) as progress:
        result = wait(futures, return_when=FIRST_COMPLETED)
        progress.update(len(result.done))
        while result.not_done:
            result = wait(result.not_done, return_when=FIRST_COMPLETED)
            progress.update(len(result.done))


if __name__ == "__main__":
    assert len(sys.argv) > 1
    main(sys.argv[1], 'dataset')
