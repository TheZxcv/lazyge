#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import pandas as pd
import subprocess
import sys
import concurrent

from tqdm import tqdm
from os import cpu_count, path, getenv
from concurrent.futures import ThreadPoolExecutor as Pool, wait, FIRST_COMPLETED

READ_FROM_FILE = False


def run_job(args):
    process = subprocess.run(args, encoding='utf-8',
                             stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    if process.returncode == 0:
        lines = process.stdout.strip().split('\n')
        res = {
            'has solution': True,
            '#active': None,
            '#solved': None,
            '#peeled': None,
            'peel rounds': None,
            'first peel': None,
            '#dense equations': None,
        }
        for line in lines:
            if line.startswith('#active vars: '):
                res['#active'] = int(line[len('#active vars: '):])
            elif line.startswith('#solved vars: '):
                res['#solved'] = int(line[len('#solved vars: '):])
            elif line.startswith('#peeled vars: '):
                res['#peeled'] = int(line[len('#peeled vars: '):])
            elif line.startswith('Peeling rounds: '):
                rest = line[len('Peeling rounds: '):].strip()
                res['peel rounds'] = rest.count(':')
                if len(rest) == 0:
                    res['first peel'] = 0
                else:
                    peel_round, peeled = rest.split()[0].split(':')
                    # if the graph was initially a 2-core, no peeling
                    # happened in the first round so it is not in the list
                    # of rounds
                    if peel_round == '1':
                        res['first peel'] = int(peeled)
                    else:
                        # the first peel was not included
                        res['first peel'] = 0
                        res['peel rounds'] += 1
            elif line.startswith('#dense equations: '):
                res['#dense equations'] = int(
                    line[len('#dense equations: '):])
            elif line.startswith('impossible'):
                res['has solution'] = False
        return (True, res)
    else:
        print(process.stdout)
        print(process.stderr)
        return (False, {})


def run_all(dataset, basedir):
    df = pd.read_csv(dataset, ',')

    df['#active'] = [float('nan')]*len(df)
    df['#solved'] = [float('nan')]*len(df)
    df['#dense equations'] = [float('nan')]*len(df)
    df['peel rounds'] = [float('nan')]*len(df)
    df['first peel'] = [float('nan')]*len(df)
    df['has solution'] = [float('nan')]*len(df)

    pool = Pool(max_workers=cpu_count())
    futures = []
    upper = len(df)
    for irow in range(0, upper):
        row = df.iloc[irow]
        if READ_FROM_FILE:
            args = ['./lge', path.join(basedir, row.filename)]
        else:
            args = ['./lge', '-r', row.rnd_options]
        if row['is sparse']:
            args.append('-S')
        future = pool.submit(run_job, args)
        future.jid = irow
        futures.append(future)

    print('Executing all jobs...')
    with tqdm(total=len(futures)) as progress:
        for future in concurrent.futures.as_completed(futures):
            progress.update(1)
            irow = future.jid
            success, data = future.result()
            if success:
                df.loc[irow, 'has solution'] = data['has solution']
                df.loc[irow, '#active'] = data['#active']
                df.loc[irow, '#solved'] = data['#solved']
                df.loc[irow, '#peeled'] = data['#peeled']
                df.loc[irow, 'peel rounds'] = data['peel rounds']
                df.loc[irow, 'first peel'] = data['first peel']
                df.loc[irow, '#dense equations'] = data['#dense equations']
                df.loc[irow, 'has solution'] = data['has solution']
    print('done!')
    df.to_csv('results.csv', index=False)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--systems-from-file',
                        action='store_true',
                        help='Read systems from specified files')
    parser.add_argument('-b', '--base-dir', type=str,
                        required=True,
                        help='Base directory')
    parser.add_argument('datasets', type=str,
                        nargs='+',
                        help='CSV files')
    args = parser.parse_args()

    global READ_FROM_FILE
    READ_FROM_FILE = args.systems_from_file
    for dataset in args.datasets:
        run_all(dataset, args.base_dir)


if __name__ == "__main__":
    main()
