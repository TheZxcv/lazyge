#!/usr/bin/env bash
set -euo pipefail

DO_GENERATE="${DO_GENERATE:-false}"
# allowed kinds are: V: var count, F: factor
KIND="${KIND:-V}"

./clean.sh

mkdir -p ./dataset/

# Check if symlink exists otherwise create it
if ! command -v ./lge > /dev/null; then
	if ! command -v ../release/lge > /dev/null; then
		echo "Please compile lge first"
		exit 1
	fi
	ln -s ../release/lge ./lge
fi

echo "Generating dataset..."

if [[ "$KIND" = "V" ]]; then
	## by var_count
	time ./generate-dataset.py -b 60 -c 1.089 --var-count 3 --sizes 500 1000 2000 2500 5000 10000 -d dataset -o ../files3.csv
	time ./generate-dataset.py -b 60 -c 1.024 --var-count 4 --sizes 500 1000 2000 2500 5000 10000 -d dataset -o ../files4.csv
	time ./generate-dataset.py -b 60 -c 1.008 --var-count 5 --sizes 500 1000 2000 2500 5000 10000 -d dataset -o ../files5.csv
else
	## by factor
	time ./generate-dataset.py -b 100 --dmodulo 127 -c 1.2 --factor 4 --sizes 500 1000 2000 2500 5000 10000 -d dataset -o ../files3.csv
	time ./generate-dataset.py -b 100 --dmodulo 127 -c 1.2 --factor 5 --sizes 500 1000 2000 2500 5000 10000 -d dataset -o ../files4.csv
	time ./generate-dataset.py -b 100 --dmodulo 127 -c 1.2 --factor 6 --sizes 500 1000 2000 2500 5000 10000 -d dataset -o ../files5.csv
fi

# Concat CSV's
cp ./files3.csv ./files.csv
{
	tail -n +2 ./files4.csv;
	tail -n +2 ./files5.csv;
} >> ./files.csv

if [[ "$DO_GENERATE" = true ]]; then
	echo "Generating instances..."
	time ./generate-instances.py files.csv
fi
echo "Running instances..."
if ! [[ "$DO_GENERATE" = true ]]; then
	time ./run-dataset.py files.csv -b .
else
	time ./run-dataset.py --systems-from-file files.csv -b .
fi
