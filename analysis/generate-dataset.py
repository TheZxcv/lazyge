#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import pandas as pd
import random

from os import path
from tqdm import tqdm
from math import floor


def create_filename(neqs, nvars, modulo, var_count, factor, sparse, is_count, seed):
    storage = 'S' if sparse else 'D'
    kind = 'C' if is_count else 'D'
    param = var_count if is_count else factor
    return f'{storage}{kind}{neqs}-{nvars}-{modulo}_{param}_{seed}.txt'


def generate_instances(equations, basedir=None, nrows=20, modulo=None, c=1.23, var_count=3, factor=3, sparse=True, is_count=True, seed=42):
    random.seed(seed)
    rows = []
    for _ in range(nrows):
        seed = random.randint(0, 2**32-1)
        neqs = equations
        nvars = floor(c * neqs)

        filename = create_filename(
            neqs, nvars, modulo, var_count, factor, sparse, is_count, seed)
        filepath = path.join(basedir, filename)

        data = {}
        data['filename'] = filepath
        data['seed'] = seed
        data['equations'] = neqs
        data['variables'] = nvars
        data['modulo'] = modulo
        data['c'] = c
        data['is sparse'] = sparse
        data['construction'] = 'C' if is_count else 'D'
        if is_count:
            data['var_count'] = var_count
            data['factor'] = None
        else:
            data['var_count'] = None
            data['factor'] = factor

        if is_count:
            options = f'seed={seed},modulo={modulo},equations={neqs},vars={nvars},var_count={var_count}'
        else:
            options = f'seed={seed},modulo={modulo},equations={neqs},vars={nvars},factor={factor}'
        data['rnd_options'] = options
        rows.append(data)
    return rows


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--var-count', type=int, default=None,
                        help='non-zero coefficients for equations')
    parser.add_argument('--factor', type=int, default=None,
                        help='Density of system matrix')
    parser.add_argument('--dmodulo', type=int, default=1021,
                        help='Modulo for dense equations')
    parser.add_argument('-c', type=float, default=1.23,
                        help='Variables-equations ratio')
    parser.add_argument('-d', '--destination', type=str,
                        required=True,
                        help='Output folder')
    parser.add_argument('-o', '--output', type=str,
                        required=True,
                        help='Output CSV file')
    parser.add_argument('-b', '--batch', type=int, default=20,
                        help='Batch size')
    parser.add_argument('-s', '--sizes', nargs='+',
                        type=int, required=True,
                        help='Sizes to generate')
    args = parser.parse_args()

    is_count = args.var_count is not None
    if is_count:
        assert args.factor is None
    assert not is_count
    instances = []
    with tqdm(total=len(args.sizes)*2*args.batch) as progress:
        for size in args.sizes:
            rows = generate_instances(size, seed=42, basedir=args.destination, nrows=args.batch, modulo=2, c=args.c,
                                      var_count=args.var_count, factor=args.factor, sparse=True, is_count=is_count)
            instances.extend(rows)
            progress.update(len(rows))

            if size < 5000:
                rows = generate_instances(size, seed=42, basedir=args.destination, nrows=args.batch, modulo=args.dmodulo, c=args.c,
                                          var_count=args.var_count, factor=args.factor, sparse=False, is_count=is_count)
                instances.extend(rows)
            progress.update(len(rows))

    random.shuffle(instances)
    df = pd.DataFrame(instances)
    filepath = path.join(args.destination, args.output)
    df.to_csv(filepath, sep=',', index=False)


if __name__ == "__main__":
    main()
