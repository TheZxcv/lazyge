#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sys
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import numpy as np
import re
import math
import seaborn as sns


def show():
    plt.show()
    plt.clf()
    plt.cla()
    plt.close()


def savefig(filename):
    plt.tight_layout(rect=[0, 0.03, 1, 0.95])
    plt.savefig(filename, transparent=True)


df = sns.load_dataset('tips')

# example: ./plots.py results.csv
dataset = sys.argv[1]

df = pd.read_csv(dataset, ',')

print(df['has solution'].value_counts())

# Ignore impossible systems
df = df[df['has solution']]
df['eq ratio'] = (df['#dense equations'] / df['equations'])
df['#dense equations'] /= df['equations']
df['#active'] /= df['variables']
df['#active'] *= 100

df['#solved'] /= df['variables']
df['#solved'] *= 100

df['#peeled'] /= df['variables']
df['#peeled'] *= 100
df['peel rounds'] -= 1

impossibles = df[df['has solution'] == False]
df = df[df['has solution']]


def print_perc_active(df, title, ax=None):
    ax = ax or plt.gca()
    res = df['eq ratio']
    res.hist(ax=ax, grid=True, bins='auto', density=True,
             color='xkcd:blue', alpha=0.7, rwidth=0.85)
    try:
        res.plot.kde(ax=ax, legend=False, color='xkcd:orange')
    except:
        pass
    ax.grid(True)
    ax.set_xlabel('Active variables')
    ax.set_ylabel('Frequency')
    ax.set_title(title)


def plot_peel_rounds(df, desc='all', ax=None):
    ax = ax or plt.gca()
    # Frequency plot for peeling rounds
    df['peel rounds'].hist(ax=ax, bins='auto', density=True,
                           color='xkcd:blue', alpha=0.7, rwidth=0.85)
    try:
        df['peel rounds'].plot.kde(ax=ax, legend=False, color='xkcd:orange')
    except:
        pass
    for axis in [ax.xaxis, ax.yaxis]:
        axis.set_major_locator(ticker.MaxNLocator(integer=True))
    ax.set_title(f'Peeling Rounds ({desc})')
    ax.grid(True)
    ax.set_xlabel('Rounds')
    ax.set_ylabel('Frequency')
    ax.set_ybound(lower=0)


colors = ['xkcd:azure', 'xkcd:coral',
          'xkcd:green', 'xkcd:indigo', 'xkcd:magenta', 'xkcd:yellow']


def plot_by_group(df, title, plt):
    # _, ax = plt.subplots()
    # ax = df['eq ratio'].hist(by=df['equations'], bins=20, stacked=False, layout=(3, 2),
    #                         density=False, ax=ax)

    groups = df.groupby('equations')
    ncols = 3
    nrows = math.ceil(len(groups) / ncols)
    fig = plt.figure()
    upper_y = 0
    axes = []
    #fig, axes = plt.subplots(nrows=2, ncols=math.ceil(len(groups) / 2))
    # for i, ((size, group), ax) in enumerate(zip(groups, axes.flatten())):
    for i, (size, group) in enumerate(groups):
        ax = fig.add_subplot(nrows, ncols, i + 1)
        axes.append(ax)
        group.hist('eq ratio', density=True, alpha=0.55,  # weights=np.ones(len(group)) * (1/len(group)),
                   stacked=False, label=size, color=colors[i], ax=ax, bins=20)
        try:
            group['eq ratio'].plot.kde(ax=ax, legend=False, color='xkcd:black')
        except:
            pass
        ax.set_title(str(size))

    upper_y = max(map(lambda ax: ax.get_ybound()[1], axes))
    upper_x = max(map(lambda ax: ax.get_xbound()[1], axes))
    for ax in axes:
        ax.grid(True)
        ax.set_ybound(lower=0, upper=upper_y)
        ax.set_xbound(lower=0, upper=upper_x)
        ax.set_xlabel('Dense equations')
        ax.set_ylabel('Frequency')
    fig.suptitle(title)


plot_peel_rounds(df)
savefig('peeling-rounds_all.pdf')
show()


# Construction by var count
d = df[df['construction'] == 'C']
plot_peel_rounds(d, desc='by var count')
savefig('peeling-rounds_var-count-all.pdf')
show()

fig, axes = plt.subplots(nrows=1, ncols=3)
d[d['var_count'] == 3].boxplot(
    '#active', by='equations', patch_artist=True, vert=True, return_type='axes', ax=axes[0])
d[d['var_count'] == 4].boxplot(
    '#active', by='equations', patch_artist=True, vert=True, return_type='axes', ax=axes[1])
d[d['var_count'] == 5].boxplot(
    '#active', by='equations', patch_artist=True, vert=True, return_type='axes', ax=axes[2])
lower_y = min(map(lambda ax: ax.get_ybound()[0], axes))
upper_y = max(map(lambda ax: ax.get_ybound()[1], axes))
for ax in axes:
    ax.grid(True)
    ax.set_ybound(lower=lower_y, upper=upper_y)
plt.tight_layout()
savefig('test.pdf')
show()

ax = sns.boxplot(x='equations', y='#active',
                 hue='var_count', data=df, palette='Set2')
ax.set(xlabel='Equations', ylabel='% of actives')
ax.grid(True)
ax.legend().set_title('$k$')
plt.tight_layout()
savefig('actives.pdf')
show()

ax = sns.boxplot(x='equations', y='#peeled',
                 hue='var_count', data=df, palette='Set2')
ax.set(xlabel='Equations', ylabel='% of peeled')
ax.grid(True)
ax.legend().set_title('$k$')
plt.tight_layout()
savefig('peeled.pdf')
show()

ax = sns.boxplot(x='equations', y='#solved',
                 hue='var_count', data=df, palette='Set2')
ax.set(xlabel='Equations', ylabel='% of solved')
ax.grid(True)
ax.legend().set_title('$k$')
plt.tight_layout()
savefig('solved.pdf')
show()

groups = d.groupby('var_count')
for (cnt, group) in groups:
    d = group
    plot_peel_rounds(d, desc=f'by var count: {int(cnt)}')
    savefig(f'peeling-rounds_var-count-{int(cnt)}.pdf')
    show()

    title = f'Construction by fixed var count: {int(cnt)}'
    plot_by_group(d, title, plt)
    savefig(f'dense-eqs_var-count-{int(cnt)}.pdf')
    show()

    print_perc_active(d, title)
    savefig(f'active-vars_var-count-{int(cnt)}.pdf')
    show()

# Construction by factor
d = df[df['construction'] == 'D']
plot_peel_rounds(d, desc='by factor')
savefig('peeling-rounds_factor-all.pdf')
show()

groups = d.groupby('factor')
for (factor, group) in groups:
    d = group
    plot_peel_rounds(d, desc=f'by factor: {factor}')
    savefig(f'peeling-rounds_factor-{factor}.pdf')
    show()

    title = f'Construction by fixed factor: {factor}'
    plot_by_group(d, title, plt)
    savefig(f'dense-eqs_factor-{factor}.pdf')
    show()

    for (size, group) in d.groupby('equations'):
        print_perc_active(group, f'{size} -- {title}')
        savefig(f'active-vars_factor-{size}_{factor}.pdf')
        show()
    print_perc_active(d, title)
    savefig(f'active-vars_factor-{factor}.pdf')
    show()
