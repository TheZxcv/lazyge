FROM gcc:8

ENV CMAKE_VERSION 3.14
ENV CMAKE_BUILD 1

# Build and install CMake
RUN    mkdir ~/temp                                                                          \
    && cd ~/temp                                                                             \
    && wget https://cmake.org/files/v$CMAKE_VERSION/cmake-$CMAKE_VERSION.$CMAKE_BUILD.tar.gz \
    && tar -xzvf cmake-$CMAKE_VERSION.$CMAKE_BUILD.tar.gz                                    \
    && cd cmake-$CMAKE_VERSION.$CMAKE_BUILD/                                                 \
    && ./bootstrap -- -DCMAKE_BUILD_TYPE:STRING=Release                                      \
    && make -j4                                                                              \
    && make install
ENV PATH /usr/local/bin:$PATH

# Install GoogleTest and build library
RUN apt-get update
RUN apt-get -y install libgtest-dev
RUN ( cd /usr/src/gtest; cmake CMakeLists.txt && make && cp *.a /usr/lib )

# Install lcov for code coverage
RUN apt-get -y install lcov

# Clean up
RUN rm -rf ~/temp
RUN rm -rf /var/lib/apt/lists/*
