#include "cli.h"

#include <vector>

#include <gtest/gtest.h>
#include <sstream>

namespace {

// `getsubopt` called inside `cli::parse_args` is going to modify all
// suboptions, so we need to make a copy of all constant strings otherwise we
// get a segfault.
static std::vector<char *> make_args(std::initializer_list<std::string> list) {
  std::vector<char *> args{};
  for (const auto &el : list) {
    char *arg = new char[el.size() + 1];
    std::copy_n(el.c_str(), el.size() + 1, arg);
    args.push_back(arg);
  }
  args.push_back(nullptr);

  return args;
}

static void destroy_args(std::vector<char *> &args) {
  while (!args.empty()) {
    auto ptr = args.back();
    delete[] ptr;
    args.pop_back();
  }
}

TEST(CliOptionsTest, filename) {
  auto args = make_args({"name", "filename"});
  auto options = cli::parse_args(args.size() - 1, args.data());

  EXPECT_EQ("filename", options.input_file);
  EXPECT_TRUE(options.solve);
  destroy_args(args);
}

TEST(CliOptionsDeathTest, filename_required) {
  auto args = make_args({"name", "-v"});
  ASSERT_DEATH(cli::parse_args(args.size() - 1, args.data()),
               "No system provided");
  destroy_args(args);
}

TEST(CliOptionsTest, enable_verbose) {
  auto args = make_args({"name", "-v", "filename"});
  auto options = cli::parse_args(args.size() - 1, args.data());

  EXPECT_TRUE(options.verbose);
  destroy_args(args);
}

TEST(CliOptionsTest, enable_dump_only) {
  auto args = make_args({"name", "-D", "filename"});
  auto options = cli::parse_args(args.size() - 1, args.data());

  EXPECT_FALSE(options.solve);
  destroy_args(args);
}

TEST(CliOptionsTest, dump_filename) {
  auto args = make_args({"name", "-d", "dump", "filename"});
  auto options = cli::parse_args(args.size() - 1, args.data());

  EXPECT_TRUE(options.dump);
  EXPECT_EQ("dump", options.dump_file);
  EXPECT_EQ("filename", options.input_file);
  destroy_args(args);
}

TEST(CliOptionsTest, enable_sparse) {
  auto args = make_args({"name", "-S", "filename"});
  auto options = cli::parse_args(args.size() - 1, args.data());

  EXPECT_TRUE(options.sparse);
  destroy_args(args);
}

TEST(CliOptionsTest, solver_flag) {
  {
    auto args = make_args({"name", "-s", "lge", "filename"});
    auto options = cli::parse_args(args.size() - 1, args.data());

    EXPECT_EQ(cli::SOLVER::LGE, options.solver);
    destroy_args(args);
  }

  {
    auto args = make_args({"name", "-s", "ge", "filename"});
    auto options = cli::parse_args(args.size() - 1, args.data());

    EXPECT_EQ(cli::SOLVER::GE, options.solver);
    destroy_args(args);
  }
}

TEST(CliOptionsTest, random_flag) {
  auto args = make_args({"name", "-r", "seed=1234,equations=1000,vars=1000"});
  auto options = cli::parse_args(args.size() - 1, args.data());

  ASSERT_TRUE(options.random_system);
  EXPECT_EQ(1234, options.random_opts.seed);
  EXPECT_EQ(2, options.random_opts.modulo);
  EXPECT_EQ(1000, options.random_opts.nequations);
  EXPECT_EQ(1000, options.random_opts.nvariables);
  EXPECT_EQ(cli::RANDOM_GEN_MODE::EXACT_COUNT, options.random_opts.mode);
  destroy_args(args);
}

TEST(CliOptionsTest, random_flag_sparse) {
  auto args =
      make_args({"name", "-S", "-r", "seed=1234,equations=1000,vars=1000"});
  auto options = cli::parse_args(args.size() - 1, args.data());

  EXPECT_TRUE(options.sparse);
  ASSERT_TRUE(options.random_system);
  EXPECT_EQ(1234, options.random_opts.seed);
  EXPECT_EQ(2, options.random_opts.modulo);
  EXPECT_EQ(1000, options.random_opts.nequations);
  EXPECT_EQ(1000, options.random_opts.nvariables);
  EXPECT_EQ(cli::RANDOM_GEN_MODE::EXACT_COUNT, options.random_opts.mode);
  destroy_args(args);
}

TEST(CliOptionsTest, random_flag_with_modulo) {
  auto args = make_args({"name", "-r", "modulo=5,equations=1000"});
  auto options = cli::parse_args(args.size() - 1, args.data());

  ASSERT_TRUE(options.random_system);
  EXPECT_EQ(5, options.random_opts.modulo);
  destroy_args(args);
}

TEST(CliOptionsTest, random_flag_count) {
  auto args = make_args({"name", "-r", "seed=1234,equations=1000,var_count=5"});
  auto options = cli::parse_args(args.size() - 1, args.data());

  ASSERT_TRUE(options.random_system);
  EXPECT_EQ(1234, options.random_opts.seed);
  EXPECT_EQ(1000, options.random_opts.nequations);
  EXPECT_EQ(cli::RANDOM_GEN_MODE::EXACT_COUNT, options.random_opts.mode);
  EXPECT_EQ(5, options.random_opts.exact_var_count);
  destroy_args(args);
}

TEST(CliOptionsTest, random_flag_factor) {
  auto args = make_args({"name", "-r", "seed=1234,equations=1000,factor=2"});
  auto options = cli::parse_args(args.size() - 1, args.data());

  ASSERT_TRUE(options.random_system);
  EXPECT_EQ(1234, options.random_opts.seed);
  EXPECT_EQ(1000, options.random_opts.nequations);
  EXPECT_EQ(cli::RANDOM_GEN_MODE::FACTOR, options.random_opts.mode);
  EXPECT_DOUBLE_EQ(2, options.random_opts.factor);
  destroy_args(args);
}

TEST(CliOptionsTest, random_flag_c) {
  auto args = make_args({"name", "-r", "seed=1234,equations=1000,c=1.024"});
  auto options = cli::parse_args(args.size() - 1, args.data());

  ASSERT_TRUE(options.random_system);
  EXPECT_EQ(1234, options.random_opts.seed);
  EXPECT_EQ(1000, options.random_opts.nequations);
  EXPECT_DOUBLE_EQ(1.024, options.random_opts.c);
  destroy_args(args);
}

TEST(CliOptionsDeathTest, random_no_factor_after_count) {
  auto args = make_args({"name", "-r", "var_count=5,factor=2"});
  ASSERT_DEATH(cli::parse_args(args.size() - 1, args.data()), "");
  destroy_args(args);
}

TEST(CliOptionsDeathTest, random_no_count_after_factor) {
  auto args = make_args({"name", "-r", "factor=2,var_count=5"});
  ASSERT_DEATH(cli::parse_args(args.size() - 1, args.data()), "");
  destroy_args(args);
}

TEST(CliOptionsTest, example_provided) {
  auto args =
      make_args({"name", "-S", "-r",
                 "seed=0,equations=1000,vars=2000,var_count=4", "INPUT-FILE"});
  auto options = cli::parse_args(args.size() - 1, args.data());

  EXPECT_TRUE(options.sparse);
  ASSERT_TRUE(options.random_system);
  EXPECT_EQ(0, options.random_opts.seed);
  EXPECT_EQ(1000, options.random_opts.nequations);
  EXPECT_EQ(2000, options.random_opts.nvariables);
  EXPECT_EQ(cli::RANDOM_GEN_MODE::EXACT_COUNT, options.random_opts.mode);
  EXPECT_EQ(4, options.random_opts.exact_var_count);
  EXPECT_EQ("INPUT-FILE", options.input_file);
  destroy_args(args);
}

TEST(CliOptionsDeathTest, print_usage) {
  auto args = make_args({"name", "-h"});
  ASSERT_EXIT(cli::parse_args(args.size() - 1, args.data()),
              ::testing::ExitedWithCode(0), "");
  destroy_args(args);
}

} // namespace
