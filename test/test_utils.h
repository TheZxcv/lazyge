#ifndef LGE_TEST_UTILS_H
#define LGE_TEST_UTILS_H

#include <gtest/gtest.h>
namespace test {

template <ModularSystemKind kind>
static void assert_non_null_solution(
    const typename mod_system<kind>::solution_type &solution) {
  ASSERT_EQ(SolutionKind::SOLVED, solution.kind);

  bool non_zero = false;
  for (auto el : solution.vars) {
    if (el != static_cast<typename mod_system<kind>::value_type>(0)) {
      non_zero = true;
    }
  }

  ASSERT_TRUE(non_zero);
}

} // namespace test

#endif // LGE_TEST_UTILS_H
