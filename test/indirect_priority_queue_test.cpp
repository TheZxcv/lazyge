#include "indirect_priority_queue.h"

#include <gtest/gtest.h>

namespace {

TEST(IndirectPriorityQueueTest, constructor) {
  const std::vector<int> v{4, 3, 2, 1};
  auto queue = queue::indirect_priority_queue<int, std::vector<int>>{v};

  ASSERT_FALSE(queue.empty());
  EXPECT_EQ(3, queue.top());
}

TEST(IndirectPriorityQueueTest, dequeue) {
  std::vector<int> v{4, 3, 2, 1};
  auto queue = queue::indirect_priority_queue<int, std::vector<int>>{v};

  ASSERT_FALSE(queue.empty());
  EXPECT_EQ(3, queue.dequeue());
  EXPECT_EQ(2, queue.dequeue());
  EXPECT_EQ(1, queue.dequeue());
  EXPECT_EQ(0, queue.dequeue());
  ASSERT_TRUE(queue.empty());
}

TEST(IndirectPriorityQueueTest, changed) {
  std::vector<int> v{4, 3, 2, 1};
  auto queue = queue::indirect_priority_queue<int, std::vector<int>>{v};

  ASSERT_FALSE(queue.empty());
  EXPECT_EQ(3, queue.dequeue());
  EXPECT_EQ(2, queue.dequeue());

  v[0] = 2;
  queue.changed(0);
  EXPECT_EQ(0, queue.dequeue());
  EXPECT_EQ(1, queue.dequeue());
  ASSERT_TRUE(queue.empty());
}

TEST(IndirectPriorityQueueTest, all_changed) {
  std::vector<int> v{4, 3, 2, 1};
  auto queue = queue::indirect_priority_queue<int, std::vector<int>>{v};

  ASSERT_FALSE(queue.empty());
  EXPECT_EQ(3, queue.dequeue());
  EXPECT_EQ(2, queue.dequeue());

  v[0] = 3;
  v[1] = 1;
  v[2] = 2;
  v[3] = 4;
  queue.all_changed();
  EXPECT_EQ(1, queue.dequeue());
  EXPECT_EQ(0, queue.dequeue());
  ASSERT_TRUE(queue.empty());
}

TEST(IndirectPriorityQueueTest, remove) {
  std::vector<int> v{4, 3, 2, 1};
  auto queue = queue::indirect_priority_queue<int, std::vector<int>>{v};

  ASSERT_FALSE(queue.empty());
  EXPECT_EQ(3, queue.dequeue());
  EXPECT_EQ(2, queue.dequeue());

  ASSERT_TRUE(queue.remove(0));

  EXPECT_EQ(1, queue.dequeue());
  ASSERT_TRUE(queue.empty());
}

TEST(IndirectPriorityQueueTest, double_remove) {
  std::vector<int> v{4, 3, 2, 1};
  auto queue = queue::indirect_priority_queue<int, std::vector<int>>{v};

  ASSERT_FALSE(queue.empty());
  EXPECT_EQ(3, queue.dequeue());
  EXPECT_EQ(2, queue.dequeue());

  ASSERT_TRUE(queue.remove(0));
  EXPECT_FALSE(queue.remove(0));

  EXPECT_EQ(1, queue.dequeue());
  ASSERT_TRUE(queue.empty());
}

} // namespace
