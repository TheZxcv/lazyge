#include "utils.h"

#include <gtest/gtest.h>
#include <sstream>

namespace {

TEST(UtilsTest, change_extension_normal) {
  auto res = change_extension("path/to/file.old_ext", "new_ext");
  EXPECT_EQ("path/to/file.new_ext", res);
}

TEST(UtilsTest, change_extension_trailing_dot) {
  auto res = change_extension("path/to/file.", "new_ext");
  EXPECT_EQ("path/to/file.new_ext", res);
}

TEST(UtilsTest, change_extension_no_ext) {
  auto res = change_extension("path/to/file", "new_ext");
  EXPECT_EQ("path/to/file.new_ext", res);
}

TEST(UtilsTest, change_extension_prev_dot_and_no_ext) {
  auto res = change_extension("path.to/file", "new_ext");
  EXPECT_EQ("path.to/file.new_ext", res);
}

TEST(UtilsTest, change_extension_only_filename) {
  EXPECT_EQ("file.new_ext", change_extension("file.old_ext", "new_ext"));
  EXPECT_EQ("file.new_ext", change_extension("file.", "new_ext"));
  EXPECT_EQ("file.new_ext", change_extension("file", "new_ext"));
}

TEST(UtilsTest, dedup_hasduplicates) {
  std::vector<int> v{1, 2, 2, 3};
  const std::vector<int> expected{1, 2, 3};

  EXPECT_TRUE(dedup(v));
  EXPECT_EQ(expected, v);
}

TEST(UtilsTest, dedup_noduplicates) {
  std::vector<int> v{1, 2, 3};
  const std::vector<int> expected{1, 2, 3};

  EXPECT_FALSE(dedup(v));
  EXPECT_EQ(expected, v);
}

} // namespace
