#include "modular_system/modular_system.h"

#include <gtest/gtest.h>
#include <sstream>

namespace {

TEST(SystemParserTest, ok) {
  std::stringstream ss{"h 3 2 1\n"
                       "e 1 2 0"};
  const auto system = read_system<ModularSystemKind::MODN>(ss);
  EXPECT_EQ(3, system.modulo);
  EXPECT_EQ(2, system.nvars);
  ASSERT_EQ(1, system.nequations);
  ASSERT_EQ(1, system.equations.size());
  ASSERT_EQ(3, system.equations[0].size());

  EXPECT_EQ(1, system.equations[0][0]);
  EXPECT_EQ(2, system.equations[0][1]);
  EXPECT_EQ(0, system.equations[0][2]);
}

TEST(SystemParserDeathTest, duplicated_header) {
  std::stringstream ss{"h 2 1 1\nh 2 1 1"};
  ASSERT_DEATH(read_system<ModularSystemKind::MODN>(ss), "duplicated header");
}

TEST(SystemParserDeathTest, equation_before_header) {
  std::stringstream ss{"e 1 1"};
  ASSERT_DEATH(read_system<ModularSystemKind::MODN>(ss),
               "equation before header");
}

TEST(SystemParserDeathTest, too_many_equations) {
  std::stringstream ss{"h 2 1 1\n"
                       "e 1 1\n"
                       "e 1 1"};
  ASSERT_DEATH(read_system<ModularSystemKind::MODN>(ss),
               "more equations than expected");
}

TEST(SystemParserDeathTest, not_enough_equations) {
  std::stringstream ss{"h 2 1 2\n"
                       "e 1 1"};
  ASSERT_DEATH(read_system<ModularSystemKind::MODN>(ss),
               "read less equations than expected");
}

TEST(SystemParserDeathTest, not_enough_coefficients) {
  std::stringstream ss{"h 2 2 1\n"
                       "e 1 1"};
  ASSERT_DEATH(read_system<ModularSystemKind::MODN>(ss),
               "failed to read equation");
}

TEST(SystemParserDeathTest, too_many_coefficients) {
  std::stringstream ss{"h 2 1 1\n"
                       "e 1 1 a"};
  ASSERT_DEATH(read_system<ModularSystemKind::MODN>(ss),
               "unexpected character after equation");
}

TEST(SystemParserDeathTest, invalid_coefficients) {
  std::stringstream ss{"h 2 1 1\n"
                       "e 3 1"};
  ASSERT_DEATH(read_system<ModularSystemKind::MODN>(ss),
               "invalid coefficient value: `3`");
}

TEST(SystemParserDeathTest, unexpected_character) {
  std::stringstream ss{"h 2 2 1\n"
                       "e 1 a"};
  ASSERT_DEATH(read_system<ModularSystemKind::MODN>(ss),
               "unexpected character");
}

TEST(SystemParserDeathTest, invalid_command_character) {
  std::stringstream ss{"d"};
  ASSERT_DEATH(read_system<ModularSystemKind::MODN>(ss),
               "invalid character `d`");
}

} // namespace
