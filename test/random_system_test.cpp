#include "modular_system/random_system.h"

#include <gtest/gtest.h>
#include <sstream>

namespace {

TEST(RandomSystemTest, by_count_equivalence) {
  unsigned long modulo{2};
  auto seeds = {1U, 2U, 3U, 4U};
  auto sizes = {500U, 1000U, 2000U, 2500U};
  for (size_t seed : seeds) {
    for (size_t neqs : sizes) {
      auto nvars = static_cast<size_t>(1.2 * neqs);

      auto engine = std::mt19937{seed};
      const auto systemGf2 =
          random_mod_system_by_count<ModularSystemKind::MOD2>(modulo, neqs,
                                                              nvars, 3, engine);

      engine.seed(seed); // reset PRNG
      const auto systemGfq =
          random_mod_system_by_count<ModularSystemKind::MODN>(modulo, neqs,
                                                              nvars, 3, engine);

      for (auto i = 0U; i < neqs; i++) {
        for (auto j = 0U; j < nvars; j++) {
          EXPECT_EQ(systemGf2.is_coeff_zero(i, j),
                    systemGfq.is_coeff_zero(i, j));
        }
      }
    }
  }
}

TEST(RandomSystemTest, by_factor_equivalence) {
  unsigned long modulo{2};
  auto seeds = {1U, 2U, 3U, 4U};
  auto sizes = {500U, 1000U, 2000U, 2500U};
  for (size_t seed : seeds) {
    for (size_t neqs : sizes) {
      auto nvars = static_cast<size_t>(1.2 * neqs);

      auto engine = std::mt19937{seed};
      const auto systemGf2 =
          random_mod_system_by_factor<ModularSystemKind::MOD2>(
              modulo, neqs, nvars, 3, engine);

      engine.seed(seed); // reset PRNG
      const auto systemGfq =
          random_mod_system_by_factor<ModularSystemKind::MODN>(
              modulo, neqs, nvars, 3, engine);

      for (auto i = 0U; i < neqs; i++) {
        for (auto j = 0U; j < nvars; j++) {
          EXPECT_EQ(systemGf2.is_coeff_zero(i, j),
                    systemGfq.is_coeff_zero(i, j));
        }
      }
    }
  }
}

} // namespace
