#include "modular_system/modular_system.h"

#include <gtest/gtest.h>
#include <sstream>

namespace {

TEST(SparseSystemParserTest, ok) {
  std::stringstream ss{"h s 2 2 1\n"
                       "e 0: 0 1"};
  const auto system = read_system<ModularSystemKind::MOD2>(ss);
  EXPECT_EQ(2, system.modulo);
  EXPECT_EQ(2, system.nvars);
  ASSERT_EQ(1, system.nequations);
  ASSERT_EQ(1, system.equations.size());
  ASSERT_EQ(2, system.equations[0].size());

  EXPECT_EQ(0, system.equations[0][0]);
  EXPECT_EQ(1, system.equations[0][1]);
}

} // namespace
