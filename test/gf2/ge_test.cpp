#include "ge.h"

#include "modular_system/modular_system.h"
#include "modular_system/random_system.h"

#include "../test_utils.h"
#include <fstream>
#include <gtest/gtest.h>

namespace {

class GeGF2ParamTest : public testing::TestWithParam<const char *> {};

TEST_P(GeGF2ParamTest, ge) {
  std::ifstream ifs{GetParam(), std::ifstream::in};
  if (ifs.fail()) {
    FAIL() << "failed to open file.";
  }

  auto setup = read_system<ModularSystemKind::MOD2>(ifs);
  auto solution = ge::gaussian_elimination_pure(setup);

  ASSERT_EQ(SolutionKind::SOLVED, solution.kind);
  test::assert_non_null_solution<ModularSystemKind::MOD2>(solution);
  EXPECT_TRUE(setup.check_solution(solution));
}

INSTANTIATE_TEST_CASE_P(
    GF2Examples, GeGF2ParamTest,
    ::testing::Values("example/sparse/sparse.txt",
                      "example/sparse/kernel_gnfs.txt",
                      "example/sparse/20-25-peelable_Z2.txt",
                      "example/sparse/2-core_peelable_Z2.txt",
                      "example/sparse/peelable.txt",
                      "example/static-functions/sf-sparse.txt"));

TEST(GeGF2Test, impossible) {
  std::ifstream ifs{"example/sparse/impossible.txt", std::ifstream::in};
  if (ifs.fail()) {
    FAIL() << "failed to open file.";
  }

  const auto system = read_system<ModularSystemKind::MOD2>(ifs);
  const auto solution = ge::gaussian_elimination_pure(system);

  EXPECT_EQ(SolutionKind::IMPOSSIBLE, solution.kind);
}

class GeGF2RandomParamTest : public testing::TestWithParam<int> {};

TEST_P(GeGF2RandomParamTest, random_system) {
  unsigned long modulo{2};
  size_t neqs = GetParam();
  std::mt19937 engine{42};
  auto nvars = static_cast<size_t>(1.2 * neqs);
  const auto system = random_mod_system_by_count<ModularSystemKind::MOD2>(
      modulo, neqs, nvars, 3, engine);

  const auto solution = ge::gaussian_elimination_pure(system);

  test::assert_non_null_solution<ModularSystemKind::MOD2>(solution);
  EXPECT_TRUE(system.check_solution(solution));
}

INSTANTIATE_TEST_CASE_P(GeGF2RandomTest, GeGF2RandomParamTest,
                        ::testing::Values(3, 4, 5, 6, 7, 8, 9, 10, 11, 10, 50,
                                          100, 200, 1000));
} // namespace
