#include "lge.h"

#include "modular_system/random_system.h"

#include "test_utils.h"
#include <fstream>
#include <gtest/gtest.h>

namespace {
class LgeParamTest : public testing::TestWithParam<const char *> {};

TEST_P(LgeParamTest, lge) {
  std::ifstream ifs{GetParam(), std::ifstream::in};
  if (ifs.fail()) {
    FAIL() << "failed to open file.";
  }

  auto system = read_system<ModularSystemKind::MODN>(ifs);
  auto solution = lge::lazy_gaussian_elimination(system);

  ASSERT_EQ(SolutionKind::SOLVED, solution.kind);
  test::assert_non_null_solution<ModularSystemKind::MODN>(solution);
  EXPECT_TRUE(system.check_solution(solution));
}

INSTANTIATE_TEST_CASE_P(
    Examples, LgeParamTest,
    ::testing::Values("example/dense/system.txt", "example/dense/system2.txt",
                      "example/dense/system3.txt", "example/dense/peelable.txt",
                      "example/dense/20-25-peelable_Z2.txt",
                      "example/dense/2-core_peelable_Z2.txt",
                      "example/dense/2-core_peelable_Z3.txt",
                      "example/dense/1000-1100-0.003.txt",
                      "example/dense/kernel_gnfs.txt",
                      "example/static-functions/sf.txt",
                      "example/static-functions/sf-gfp.txt"));

TEST(LgeTest, impossible) {
  std::ifstream ifs{"example/dense/impossible.txt", std::ifstream::in};
  if (ifs.fail()) {
    FAIL() << "failed to open file.";
  }

  const auto system = read_system<ModularSystemKind::MODN>(ifs);
  const auto solution = lge::lazy_gaussian_elimination(system);

  EXPECT_EQ(SolutionKind::IMPOSSIBLE, solution.kind);
}

class LgeRandomParamTest : public testing::TestWithParam<int> {};

TEST_P(LgeRandomParamTest, random_system) {
  mpz_class modulo{1021};
  size_t neqs = GetParam();
  std::mt19937 engine{42};
  auto nvars = static_cast<size_t>(1.5 * neqs);
  const auto system = random_mod_system_by_count<ModularSystemKind::MODN>(
      modulo, neqs, nvars, 3, engine);

  const auto solution = lge::lazy_gaussian_elimination(system);

  ASSERT_EQ(SolutionKind::SOLVED, solution.kind);
  test::assert_non_null_solution<ModularSystemKind::MODN>(solution);
  EXPECT_TRUE(system.check_solution(solution));
}

INSTANTIATE_TEST_CASE_P(LgeRandomTest, LgeRandomParamTest,
                        ::testing::Values(10, 50, 100, 200, 1000));

TEST(LgeTest, random_system_5) {
  mpz_class modulo{3};
  size_t neqs = 4;
  std::mt19937 engine{2955};
  auto nvars = static_cast<size_t>(1.5 * neqs);
  const auto system = random_mod_system_by_factor<ModularSystemKind::MODN>(
      modulo, neqs, nvars, 3, engine);

  const auto solution = lge::lazy_gaussian_elimination(system);

  ASSERT_EQ(SolutionKind::SOLVED, solution.kind);
  test::assert_non_null_solution<ModularSystemKind::MODN>(solution);
  EXPECT_TRUE(system.check_solution(solution));
}
} // namespace
