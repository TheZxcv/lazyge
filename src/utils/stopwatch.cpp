/**
 * @file stopwatch.cpp
 * @author TheZxcv
 * @brief A stopwatch class for benchmarks
 */

#include "utils/stopwatch.h"

#include <iomanip>
#include <ostream>

namespace bench {

stop_watch::stop_watch()
    : is_running{false}, start_time{chrono_clock::time_point::min()},
      elapsed_time{chrono_clock::duration::zero()}, taskname{""} {}

stop_watch::chrono_clock::duration stop_watch::elapsed_duration() const {
  if (is_running) {
    return stop_watch::chrono_clock::now() - start_time;
  } else {
    return elapsed_time;
  }
}

void stop_watch::start() {
  if (!is_running) {
    is_running = true;
    start_time = stop_watch::chrono_clock::now();
  }
}

void stop_watch::start(const std::string &name) {
  taskname = name;
  start();
}

void stop_watch::stop() {
  if (is_running) {
    elapsed_time = elapsed_duration();
    is_running = false;
  }
}

void stop_watch::reset() {
  is_running = false;
  start_time = stop_watch::chrono_clock::now();
  elapsed_time = stop_watch::chrono_clock::duration(0);
}

void stop_watch::restart() {
  is_running = true;
  start_time = stop_watch::chrono_clock::now();
  elapsed_time = stop_watch::chrono_clock::duration(0);
}

void stop_watch::restart(const std::string &name) {
  taskname = name;
  restart();
}

long stop_watch::elapsed() const {
  return std::chrono::duration_cast<std::chrono::microseconds>(
             elapsed_duration())
      .count();
}

std::ostream &operator<<(std::ostream &out, const stop_watch &sw) {
  out << "[TIME] ";
  if (sw.taskname.length() > 0) {
    out << sw.taskname << " ";
  }
  return out << std::fixed << std::setprecision(2) << (sw.elapsed() / 1000.0L)
             << "ms";
}

} // namespace bench
