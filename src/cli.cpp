/**
 * @file cli.cpp
 * @author TheZxcv
 * @brief command line arguments parser
 */

#include "cli.h"

#include "utils.h"

#include <getopt.h>
#include <iostream>
#include <istream>
#include <string>

namespace cli {

static void print_usage(const std::string &program) {
  struct arg_options defaults {};
  struct random_gen_options rand_gen_defaults {};
  std::cout << "Usage: " << program
            << " [-vhSD] [-d FILE] [-s SOLVER] [-r OPTS] [FILE]" << std::endl
            << "Options and arguments:" << std::endl
            << " -v        enable verbose logging (default: "
            << defaults.verbose << ")" << std::endl
            << " -d FILE   dump system to file" << std::endl
            << " -D        dump only, do not solve the system" << std::endl
            << " -s SOLVER use the solver SOLVER (default: "
            << solver_to_string(defaults.solver) << ")."
            << " The available solvers are: " << solver_to_string(cli::LGE)
            << ", " << solver_to_string(cli::GE) << ")" << std::endl
            << " -r OPTS   ignore FILE (if provided) and generate a random "
               "system according to the comma separated options provided."
            << std::endl
            << "           Providing `factor` disables `var_count` and vice "
               "versa, by "
               "default var_count is assumed to be active."
            << std::endl
            << "           Options:" << std::endl
            << "           - seed= PRNG seed (default: "
            << rand_gen_defaults.seed << ")" << std::endl
            << "           - modulo= modulo (default: "
            << rand_gen_defaults.modulo << ")" << std::endl
            << "           - equations= number of equations (default: "
            << rand_gen_defaults.nequations << ")" << std::endl
            << "           - vars= number of variables (default: 1.5*equations)"
            << std::endl
            << "           - c= variables-equations ratio, "
               "variables=c*equations (default: "
            << rand_gen_defaults.c << ")" << std::endl
            << "           - factor= factor of non-zero coefficients (default: "
            << rand_gen_defaults.factor << ")" << std::endl
            << "           - var_count= number of non-zero coefficient for "
               "each equation (default: "
            << rand_gen_defaults.exact_var_count << ")" << std::endl
            << " -S        read input file in sparse mode" << std::endl
            << " -h        prints this usage message" << std::endl
            << std::endl
            << "Example:" << std::endl
            << "$ " << program
            << " -S -r seed=0,equations=1000,vars=2000,var_count=4" << std::endl
            << "$ " << program << " INPUT-FILE" << std::endl;
}

struct random_gen_options parse_random_flag_suboptions(char *optarg) {
  enum SUBOPTIONS {
    NEQUATIONS_OPT = 0,
    NVARIABLES_OPT,
    FACTOR_OPT,
    VAR_COUNT_OPT,
    SEED_OPT,
    MODULO_OPT,
    C_OPT,
    LAST
  };
  const char *const tokens[LAST + 1] = {"equations", "vars", "factor",
                                        "var_count", "seed", "modulo",
                                        "c",         nullptr};

  struct random_gen_options result {};
  char *subopts;
  char *value;

#define ERROR_IF_MISSING(value, opt)                                           \
  do {                                                                         \
    if (value == nullptr) {                                                    \
      std::ostringstream oss{};                                                \
      oss << "Missing value for "                                              \
             "suboption '"                                                     \
          << tokens[opt] << "'";                                               \
      error(oss.str());                                                        \
    }                                                                          \
  } while (false)

  bool seen_factor_or_count = false;
  subopts = optarg;
  while (*subopts != '\0') {
    // XXX: casting const away because `getsubopt` signature is wrong.
    auto token = getsubopt(&subopts, const_cast<char *const *>(tokens), &value);
    switch (static_cast<SUBOPTIONS>(token)) {
    case NEQUATIONS_OPT: {
      ERROR_IF_MISSING(value, NEQUATIONS_OPT);
      result.nequations = std::stoul(value);
    } break;

    case NVARIABLES_OPT: {
      ERROR_IF_MISSING(value, NVARIABLES_OPT);
      result.nvariables = std::stoul(value);
    } break;

    case FACTOR_OPT: {
      ERROR_IF_MISSING(value, FACTOR_OPT);
      if (seen_factor_or_count) {
        std::ostringstream oss{};
        oss << "cannot specify `" << tokens[FACTOR_OPT] << "` after `"
            << tokens[VAR_COUNT_OPT] << "`";
        error(oss.str());
      }
      seen_factor_or_count = true;
      result.mode = RANDOM_GEN_MODE::FACTOR;
      result.factor = std::stoul(value);
    } break;

    case VAR_COUNT_OPT: {
      ERROR_IF_MISSING(value, VAR_COUNT_OPT);
      if (seen_factor_or_count) {
        std::ostringstream oss{};
        oss << "cannot specify `" << tokens[VAR_COUNT_OPT] << "` after `"
            << tokens[FACTOR_OPT] << "`";
        error(oss.str());
      }
      seen_factor_or_count = true;
      result.mode = RANDOM_GEN_MODE::EXACT_COUNT;
      result.exact_var_count = std::stoul(value);
    } break;

    case SEED_OPT: {
      ERROR_IF_MISSING(value, SEED_OPT);
      result.seed = std::stoul(value);
    } break;

    case MODULO_OPT: {
      ERROR_IF_MISSING(value, MODULO_OPT);
      result.modulo = std::stoul(value);
    } break;

    case C_OPT: {
      ERROR_IF_MISSING(value, C_OPT);
      result.c = std::stod(value);
    } break;

    default:
      std::ostringstream oss{};
      oss << "Unknown suboption `" << value << "'.";
      error(oss.str());
    }
  }
#undef ERROR_IF_MISSING

  return result;
}

struct arg_options parse_args(int argc, char *argv[]) {
  if (argc <= 1) {
    print_usage(argv[0]);
    std::exit(EXIT_SUCCESS);
  }
  struct arg_options options {};

  int opt;
  opterr = 0;
  optind = 0; // resets `getopt`
  while ((opt = getopt(argc, argv, "s:Sr:vhd:D")) != EOF) {
    switch (opt) {
    case 's': {
      std::string solver_name{optarg};
      if (solver_name == "lge") {
        options.solver = cli::LGE;
      } else if (solver_name == "ge") {
        options.solver = cli::GE;
      } else {
        std::ostringstream oss{};
        oss << "Invalid solver `" << solver_name << "` provided.";
        error(oss.str());
      }
    } break;
    case 'r': {
      options.random_system = true;
      options.random_opts = parse_random_flag_suboptions(optarg);
    } break;
    case 'v':
      options.verbose = true;
      break;
    case 'S':
      options.sparse = true;
      break;
    case 'd':
      options.dump = true;
      options.dump_file = optarg;
      break;
    case 'D':
      options.dump = true;
      options.solve = false;
      break;
    case 'h':
      print_usage(argv[0]);
      std::exit(EXIT_SUCCESS);
    case '?':
      if (optopt == 'r' || optopt == 's' || optopt == 'd')
        std::cerr << "Option -" << static_cast<char>(optopt)
                  << " requires a value" << std::endl;
      else
        std::cerr << "Unknown option `-" << static_cast<char>(optopt) << "'."
                  << std::endl;
      print_usage(argv[0]);
      std::exit(EXIT_FAILURE);
    default:
      error("failure while parsing command line arguments.");
    }
  }
  if (argv[optind] != nullptr) {
    options.input_file = argv[optind];
  } else if (!options.random_system) {
    std::cerr << "No system provided." << std::endl;
    std::exit(EXIT_FAILURE);
  }

  return options;
}
} // namespace cli
