/**
 * @file main.cpp
 * @author TheZxcv
 * @brief lazy Gaussian Elimination executable
 */

#include "cli.h"
#include "modular_system/modular_system.h"
#include "modular_system/random_system.h"

#include "ge.h"
#include "lge.h"
#include "utils.h"
#include "utils/stopwatch.h"

#include <fstream>
#include <iostream>

template <ModularSystemKind Kind>
static mod_system<Kind> make_system(const cli::arg_options &options) {
  if (options.random_system) {
    const unsigned long seed = options.random_opts.seed;
    std::mt19937 engine{seed};
    typename mod_system<Kind>::modulo_type modulo{options.random_opts.modulo};
    size_t neqs = options.random_opts.nequations;
    auto nvars = options.random_opts.nvariables != 0
                     ? options.random_opts.nvariables
                     : static_cast<size_t>(options.random_opts.c * neqs);

    std::cout << "     seed: " << seed << std::endl
              << "   modulo: " << modulo << std::endl
              << "variables: " << nvars << std::endl
              << "equations: " << neqs << std::endl;

    if (options.random_opts.mode == cli::RANDOM_GEN_MODE::FACTOR) {
      std::cout << "   factor: " << options.random_opts.factor << std::endl;
      return random_mod_system_by_factor<Kind>(
          modulo, neqs, nvars, options.random_opts.factor, engine);
    } else {
      std::cout << "var_count: " << options.random_opts.exact_var_count
                << std::endl;
      return random_mod_system_by_count<Kind>(
          modulo, neqs, nvars, options.random_opts.exact_var_count, engine);
    }
  } else {
    std::cout << "Reading from file: " << options.input_file << std::endl;
    std::ifstream ifs{options.input_file, std::ifstream::in};
    if (ifs.fail()) {
      error("failed to open file.");
    }

    return read_system<Kind>(ifs);
  }
}

template <ModularSystemKind kind>
static void dump_system(const cli::arg_options &options,
                        const mod_system<kind> &system) {
  bench::stop_watch timer{};
  timer.start("dump_system");

  std::ofstream ofs{options.dump_file, std::ofstream::out};
  if (ofs.fail()) {
    error("failed to open file.");
  }
  write_system<kind>(ofs, system);
  timer.stop();

  std::cout << "System dumped to file: " << options.dump_file << ""
            << std::endl;

  std::cout << timer << std::endl;
}

template <ModularSystemKind Kind>
static typename mod_system<Kind>::solution_type
solve(cli::SOLVER solver, const mod_system<Kind> &system) {
  switch (solver) {
  case cli::LGE:
    return lge::lazy_gaussian_elimination<Kind>(system);
  case cli::GE:
    return ge::gaussian_elimination_pure<Kind>(system);
  default:
    error("unknown solver.");
  }
}

template <ModularSystemKind kind>
static bool
is_solution_null(const typename mod_system<kind>::solution_type &solution) {
  for (auto el : solution.vars) {
    if (el != static_cast<typename mod_system<kind>::value_type>(0)) {
      return false;
    }
  }

  return true;
}

template <ModularSystemKind kind>
static void
save_solution(const typename mod_system<kind>::solution_type &solution,
              const std::string &solution_file) {
  bench::stop_watch timer{};
  timer.start("save_solution");

  std::ofstream ofs{solution_file, std::ofstream::out};
  if (ofs.fail()) {
    error("failed to open file.");
  }

  for (const auto &value : solution.vars) {
    ofs << value << std::endl;
  }

  timer.stop();
  std::cout << "Solution saved to file: " << solution_file << std::endl;
  std::cout << timer << std::endl;
}

template <ModularSystemKind kind> int run(const cli::arg_options &options) {
  bench::stop_watch timer{};
  timer.start("make_system");
  auto system = make_system<kind>(options);
  std::cout << timer << std::endl;
  std::cout << system.summary() << std::endl;
  if (options.dump) {
    dump_system(options, system);
  }
  if (!options.solve) {
    return 0;
  }

  timer.restart("solve");
  auto solution = solve(options.solver, system);
  std::cout << timer << std::endl;

  if (solution.kind == SolutionKind::IMPOSSIBLE) {
    std::cout << "impossible" << std::endl;
  } else if (system.check_solution(solution)) {
    std::cout << "solved and check passed" << std::endl;
    std::cout << "solution vector is "
              << (is_solution_null<kind>(solution) ? "null" : "non-null")
              << std::endl;

    const std::string filename{
        options.random_system ? "solution.sol"
                              : change_extension(options.input_file, "sol")};
    save_solution<kind>(solution, filename);
  } else {
    std::cout << "solved and check FAILED" << std::endl;

    // dump it if not already done
    if (!options.dump) {
      dump_system(options, system);
    }
  }

  return 0;
}

int main(int argc, char *argv[]) {
  auto options = cli::parse_args(argc, argv);
  if (options.sparse) {
    return run<ModularSystemKind::MOD2>(options);
  } else {
    return run<ModularSystemKind::MODN>(options);
  }
}
