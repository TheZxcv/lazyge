/**
 * @file utils.cpp
 * @author TheZxcv
 * @brief Bunch of utilities
 */

#include "utils.h"

#include <iomanip>
#include <iostream>

#define ANSI_RED "\x1b[31m"
#define ANSI_RESET "\x1b[0m"

[[noreturn]] void error(const std::string &message) {
  std::cerr << ANSI_RED "Error" ANSI_RESET ": " << message << std::endl;
  std::exit(-1);
}

[[noreturn]] void error_at_line(const int lineno, const std::string &message) {
  std::stringstream ss{};
  ss << "[line:" << lineno << "]: " << message;
  error(ss.str());
}

void __debug_assert(bool condition, const std::string &message) {
  if (!condition)
    error(message);
}

inline bool is_path_separator(char c) {
#if defined(WIN32) || defined(_WIN32) || defined(__CYGWIN__)
  return c == '/' || c == '\\';
#else
  return c == '/';
#endif
}

std::string change_extension(const std::string &filepath,
                             const std::string &new_ext) {
  bool found_dot = false;
  size_t new_end = filepath.size();
  for (auto it = filepath.crbegin(); it != filepath.crend(); ++it) {
    new_end--;
    if (is_path_separator(*it)) {
      // the file does not have an extension
      break;
    }
    if (*it == '.') {
      found_dot = true;
      break;
    }
  }

  if (found_dot) {
    return filepath.substr(0, new_end).append(".").append(new_ext);
  } else {
    std::string new_filepath{filepath};
    return new_filepath.append(".").append(new_ext);
  }
}

static const char *suffixes[] = {"B", "KiB", "MiB", "GiB", "TiB", "PiB", "EiB"};
std::ostream &human_readable::operator()(std::ostream &out) const {
  size_t suffix = 0;
  double readable;
  if (value >= 0x1000000000000000) {
    suffix = 6;
    readable = (value >> 50);
  } else if (value >= 0x4000000000000) {
    suffix = 5;
    readable = (value >> 40);
  } else if (value >= 0x10000000000) {
    suffix = 4;
    readable = (value >> 30);
  } else if (value >= 0x40000000) {
    suffix = 3;
    readable = (value >> 20);
  } else if (value >= 0x100000) {
    suffix = 2;
    readable = (value >> 10);
  } else if (value >= 0x400) {
    suffix = 1;
    readable = value;
  } else {
    return out << value << suffixes[0];
  }
  readable = (readable / 1024);
  return out << std::fixed << std::setprecision(2) << readable
             << suffixes[suffix];
}

std::ostream &operator<<(std::ostream &out, human_readable readable) {
  return readable(out);
}
