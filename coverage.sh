#!/bin/sh
set -e

lcov -q --no-external --capture --base-directory . --directory "$1" --output-file coverage.unfiltered
lcov -q --remove coverage.unfiltered "*/test/*" -o coverage.total
lcov --summary coverage.total

rm -f coverage.unfiltered
