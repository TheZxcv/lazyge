/**
 * @file cli.h
 * @author TheZxcv
 * @brief command line arguments parser
 */

#ifndef LGE_CLI_H
#define LGE_CLI_H

#include <string>

namespace cli {

enum SOLVER {
  LGE,
  GE,
};

inline const char *solver_to_string(SOLVER solver) {
  switch (solver) {
  case LGE:
    return "lge";
  case GE:
    return "ge";
  default:
    return "[Unknown]";
  }
}

enum class RANDOM_GEN_MODE {
  FACTOR,
  EXACT_COUNT,
};

struct random_gen_options {
  unsigned long seed = 42;
  unsigned long modulo = 2;
  RANDOM_GEN_MODE mode = RANDOM_GEN_MODE::EXACT_COUNT;
  unsigned int factor = 3;
  unsigned int exact_var_count = 3;
  double c = 1.23; // 1.089
  unsigned int nequations = 5000;
  unsigned int nvariables = 0;
};

struct arg_options {
  bool verbose = false;
  SOLVER solver = LGE;
  bool random_system = false;
  struct random_gen_options random_opts;
  bool sparse = false;
  std::string input_file;
  bool dump = false;
  std::string dump_file = "system.txt";
  bool solve = true;
};

struct arg_options parse_args(int argc, char *argv[]);

} // namespace cli

#endif
