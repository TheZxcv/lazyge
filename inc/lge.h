/**
 * @file lge.h
 * @author TheZxcv
 * @brief lazy Gaussian Elimination library
 */

#ifndef LGE_H
#define LGE_H

#include "modular_system/modular_system.h"

#include "ge.h"
#include "indirect_priority_queue.h"
#include "utils.h"

#include <algorithm>
#include <deque>
#include <iostream>
#include <numeric>
#include <utils/stopwatch.h>
#include <vector>

namespace lge {

struct peel_info {
  unsigned int npeeled;
  unsigned int round;
};
#ifdef COLLECT_STATS
unsigned int round = 0;
std::vector<peel_info> round_of_peeling{};
#endif

/**
 * Appends vector `src` to vector `dst`
 * @param dst destination vector
 * @param src vector to append
 */
template <typename T>
inline static void append(std::vector<T> &dst, const std::vector<T> &src) {
  if (!src.empty()) {
    dst.reserve(dst.size() + src.size());
    dst.insert(std::end(dst), std::begin(src), std::end(src));
  }
}

template <ModularSystemKind kind>
int peel(mod_system<kind> &system, std::vector<size_t> &&candidates,
         int &idle_vars, int &active_vars) {
  int npeeled = 0;
  while (!candidates.empty()) {
    size_t v_idx = candidates.back();
    candidates.pop_back();

    auto &v = system.var_states[v_idx];
    if (system.var2eq_cnt[v_idx] != 1 || (v.state != ACTIVE && v.state != IDLE))
      continue;

    debug_assert(system.var2eq_cnt[v_idx] == 1,
                 "variable appears in more than 1 equation");
    debug_assert((v.state == ACTIVE || v.state == IDLE),
                 "variable is not active or idle");
    auto solving_eq = system.var2xored_eqs[v_idx];
    debug_assert(solving_eq < system.eq_states.size(),
                 "solved_by out of bounds");

    if (system.eq_states[solving_eq].state != IGNORED) {
      if (v.state == IDLE) {
        idle_vars--;
      } else {
        active_vars--;
      }
      // std::cout << "Peeled: " << v << std::endl;
      // std::cout << " in " << eq_states[v.solved_by] << std::endl;
      system.stack_of_solved.emplace_back(v.idx, solving_eq);
      v.state = PEELED;
      system.eq_states[solving_eq].state = IGNORED;
      auto new_candidates = system.ignore_equation(solving_eq);
      append(candidates, new_candidates);
      npeeled++;
    }
  }

#ifdef GE_DEBUG
  // check that all peelable were peeled
  std::vector<size_t> var2non_ignored_eqs{};
  var2non_ignored_eqs.resize(static_cast<size_t>(system.nvars));
  std::fill(var2non_ignored_eqs.begin(), var2non_ignored_eqs.end(), 0);

  for (size_t eq_idx = 0; eq_idx < system.equations.size(); eq_idx++) {
    for (size_t v_idx = 0; v_idx < static_cast<size_t>(system.nvars); v_idx++) {
      if (system.eq_states[eq_idx].state != IGNORED &&
          !system.is_coeff_zero(eq_idx, v_idx)) {
        var2non_ignored_eqs[v_idx]++;
      }
    }
  }

  for (size_t v_idx = 0; v_idx < static_cast<size_t>(system.nvars); v_idx++) {
    auto &v = system.var_states[v_idx];
    if (v.state == ACTIVE || v.state == IDLE) {
      debug_assert(system.var2eq_cnt[v_idx] == var2non_ignored_eqs[v_idx],
                   "var2eq_cnt is wrong after peeling");
      debug_assert(var2non_ignored_eqs[v_idx] != 1,
                   "a peelable variable was not peeled");
    }
  }
#endif

#ifdef COLLECT_STATS
  round++;
  if (npeeled > 0) {
    round_of_peeling.push_back({static_cast<unsigned int>(npeeled), round});
  }
#endif

  return npeeled;
}

template <ModularSystemKind kind>
typename mod_system<kind>::solution_type
_lazy_gaussian_elimination(mod_system<kind> &system) {
  // Counters
  int peeled_vars = 0;
  int active_vars = 0;
  int solved_vars = 0;
  int idle_vars = static_cast<int>(system.nvars);
  bench::stop_watch timer{};

  timer.start("setup");
  {
    std::vector<size_t> candidates(system.nvars);
    std::iota(std::begin(candidates), std::end(candidates), 0);
    peeled_vars = peel(system, std::move(candidates), idle_vars, active_vars);
  }

  auto var_prio_queue =
      queue::indirect_priority_queue<size_t, std::vector<size_t>,
                                     std::greater<>>{system.var2eq_cnt};
  std::cout << timer << std::endl;

  timer.restart("lge");
  // number of equations containing only active variables
  size_t fully_dense = system.eq_states.size();
  while (idle_vars > 0) {
    debug_assert(static_cast<size_t>(idle_vars + solved_vars + active_vars +
                                     peeled_vars) == system.var_states.size(),
                 "sum of counters does not equal vars.size()");

    while (!system.zero_or_one_prio.empty()) {
      size_t eq_idx = system.zero_or_one_prio.front();
      equation_state &eq = system.eq_states[eq_idx];
      system.zero_or_one_prio.pop_front();

      // eq was inserted twice, it was already handled, skip it
      if (eq.state == DENSE) {
        continue;
      } else if (eq.prio == 0 && eq.state == SPARSE) {
        // handle 0-priority equations
        eq.state = DENSE;

        bool has_no_var = system.eq2var_cnt[eq_idx] == 0;

        // if all variables have coefficient zero and b is nonzero then
        // the equation is 0 = b which is unsolvable.
        if (has_no_var && system.constant_term(eq_idx) != 0) {
          return {SolutionKind::IMPOSSIBLE, {}};
        } else if (has_no_var) {
          // eq is an identity
          eq.state = IGNORED;
          fully_dense--;
        }
      } else if (eq.prio == 1 && eq.state == SPARSE) {
        // handle 1-priority equations
        // eq is an equation with one solved variable and all others active
        eq.state = IGNORED;
        fully_dense--;

        auto ivar =
            std::find_if(var_prio_queue.cbegin(), var_prio_queue.cend(),
                         [eq_idx, &system](auto &v_idx) {
                           return !system.is_coeff_zero(eq_idx, v_idx) &&
                                  system.var_states[v_idx].state == IDLE;
                         });
        if (ivar == var_prio_queue.cend()) {
          error("could not find IDLE variable in equation with priority 1.");
        }
        auto &var = system.var_states[*ivar];

        // std::cout << "Solve v" << var->idx << "" << std::endl;

        var.state = SOLVED;
        // std::cout << " using " << eq_states[var->solved_by] << std::endl;
        system.stack_of_solved.emplace_back(var.idx, eq_idx);
        solved_vars++;
        idle_vars--;

        auto candidates = system.substitute(var.idx, eq_idx);
        {
          auto candidates_from_ignore = system.ignore_equation(eq_idx);
          append(candidates, candidates_from_ignore);
        }

        int last_peel = 0;
        if (!candidates.empty()) {
          last_peel =
              peel(system, std::move(candidates), idle_vars, active_vars);
        }
        peeled_vars += last_peel;
        if (last_peel > 0) {
          var_prio_queue.all_changed();
        } else {
          var_prio_queue.remove(var.idx);
        }
      }
    }

    // No 0-priority or 1-priority equation was found,
    // thus we activate the heaviest IDLE variable.
    size_t heavy_idle = 0;
    bool found = false;
    while (!var_prio_queue.empty()) {
      heavy_idle = var_prio_queue.dequeue();
      if (system.var_states[heavy_idle].state == IDLE) {
        found = true;
        break;
      }
    }

    if (!found) {
      // no idle variable is left, we're done
      break;
    }

    // std::cout << "Make v" << heavy_idle << " active" << std::endl;
    auto &var = system.var_states[heavy_idle];
    var.state = ACTIVE;
    active_vars++;
    idle_vars--;

    for (size_t i = 0; i < system.nvars; i++) {
      if (system.var_states[i].state == IDLE) {
        debug_assert(system.var2eq_cnt[i] <= system.var2eq_cnt[var.idx],
                     "The variable that was activated is not the heaviest.");
      }
    }

    // lower the priority of every equations that contains
    // the variable we just made active
    size_t updated_eq_count = 0;
    for (size_t i = 0; i < system.nequations; i++) {
      if (!system.ignored_eq[i] && !system.is_coeff_zero(i, var.idx)) {
        updated_eq_count++;
        auto &eq = system.eq_states[i];
        eq.prio--;
        if (eq.prio < 0) {
          error("negative priority.");
        } else if (eq.prio <= 1 && eq.state == SPARSE) {
          const auto idx = static_cast<size_t>(&eq - &system.eq_states[0]);
          if (eq.prio == 0)
            system.zero_or_one_prio.push_front(idx);
          else
            system.zero_or_one_prio.push_back(idx);
        }

        // all equations in which `var` appears have been updated,
        // exit early
        if (updated_eq_count == system.var2eq_cnt[var.idx])
          break;
      }
    }
  }
  /*
   * All the remaining sparse equations are ignored since
   * they are not needed to solve the system.
   */

  timer.stop();
  std::cout << std::endl << timer << std::endl;
  std::cout << "#active vars: " << active_vars << std::endl;
  std::cout << "#solved vars: " << solved_vars << std::endl;
  std::cout << "#peeled vars: " << peeled_vars << std::endl;

#ifdef COLLECT_STATS
  std::cout << "Peeling rounds: ";
  for (auto &el : round_of_peeling) {
    std::cout << el.round << ":" << el.npeeled << " ";
  }
  std::cout << std::endl;
#endif

  // map from index in dense solution to index in `vars`
  size_t ivar = 0;
  std::vector<size_t> sol2var(static_cast<size_t>(active_vars));
  for (size_t idx = 0; idx < system.nvars; idx++) {
    auto &v = system.var_states[idx];
    if (v.state == ACTIVE) {
      sol2var[ivar++] = v.idx;
    }
  }

  auto dense_system = system.build_dense_system(active_vars);
  std::cout << "Dense system: " << std::endl
            << dense_system.summary() << std::endl;

#ifdef COLLECT_STATS
  std::cout << "#dense equations: " << dense_system.nequations << std::endl;
#endif

  // empty solution
  std::vector<typename mod_system<kind>::value_type> solution(system.nvars);
  std::fill(solution.begin(), solution.end(), 0);

  timer.restart("ge");
  if (dense_system.nequations != 0 && dense_system.nvars != 0) {
    auto dense_solution = ge::gaussian_elimination(dense_system);
    if (dense_solution.kind == SolutionKind::IMPOSSIBLE)
      return dense_solution;

    for (size_t i = 0; i < dense_solution.vars.size(); i++) {
      solution[sol2var[i]] = dense_solution.vars[i];
    }
  } else if (dense_system.nvars != 0) {
    // all these variables are free
    for (size_t i = 0; i < dense_system.nvars; i++) {
      solution[sol2var[i]] = 1;
    }
  }
  std::cout << timer << std::endl;

  timer.restart("solved/peeled");
  for (auto iter = system.stack_of_solved.rbegin();
       iter != system.stack_of_solved.rend(); ++iter) {
    auto v_idx = std::get<0>(*iter);
    auto eq_idx = std::get<1>(*iter);
    debug_assert(!system.is_coeff_zero(eq_idx, v_idx),
                 "Solved or peeled equation does not contain pivot variable");
    auto &v = system.var_states[v_idx];
    debug_assert(
        v.state == SOLVED || v.state == PEELED,
        "Found a variable that is neither solver or peeled in stack_of_solved");
    solution[v.idx] = system.eval_equation_for_var(solution, eq_idx, v.idx);
  }
  std::cout << timer << std::endl;

  return {SolutionKind::SOLVED, solution};
}

/**
 * Applies lazy gaussian elimination to the system of equations `system`.
 * The parameter `system` is preserved.
 * @param system the system of equations
 */
template <ModularSystemKind kind>
typename mod_system<kind>::solution_type
lazy_gaussian_elimination(const mod_system<kind> &system) {
#ifdef COLLECT_STATS
  round = 0;
  round_of_peeling.clear();
#endif
  mod_system<kind> copy{system};
  return _lazy_gaussian_elimination(copy);
}

} // namespace lge

#endif
