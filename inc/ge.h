/**
 * @file ge.h
 * @author TheZxcv
 * @brief Gaussian Elimination library
 */

#ifndef GE_H
#define GE_H

#include "modular_system/modular_system.h"
#include "utils.h"

namespace ge {

/**
 * Solves the system of equations `setup` using the classical Gaussian
 * elimination algorithm. The parameter `setup` is clobbered.
 * @param setup system of equations
 * @return the solution to the system
 */
template <ModularSystemKind Kind>
typename mod_system<Kind>::solution_type
gaussian_elimination(mod_system<Kind> &setup);

/**
 * Solves the system of equations `setup` using the classical Gaussian
 * elimination algorithm. The parameter `setup` is preserved.
 * @param setup system of equations
 * @return the solution to the system
 */
template <ModularSystemKind Kind>
typename mod_system<Kind>::solution_type
gaussian_elimination_pure(const mod_system<Kind> &setup) {
  mod_system<Kind> copy{setup};
  return gaussian_elimination(copy);
}

template <>
typename mod_system<ModularSystemKind::MOD2>::solution_type
gaussian_elimination(mod_system<ModularSystemKind::MOD2> &setup) {
  std::vector<bool> vars(setup.nvars);
  std::fill(vars.begin(), vars.end(), true);

  // setup `first_var` vector to keep track of the first variable with
  // a non-zero coefficient for each equation
  const size_t sentinel = std::numeric_limits<size_t>::max();
  debug_assert(sentinel > setup.nequations, "sentinel <= nequations");
  std::vector<size_t> first_var(setup.nequations);
  for (size_t idx = 0; idx < setup.equations.size(); idx++) {
    first_var[idx] = sentinel;
    for (size_t i = 0; i < setup.nvars; i++) {
      if (!setup.is_coeff_zero(idx, i)) {
        first_var[idx] = i;
        break;
      }
    }
  }

  size_t pivot_row = 0;
  for (size_t pivot_col = 0;
       pivot_col < setup.nvars && pivot_row < setup.nequations; pivot_col++) {

    // find the first row with a non-zero value on the column `pivot_col`
    size_t selected_row = pivot_row;
    bool is_zero = first_var[pivot_row] != pivot_col;
    for (size_t row = pivot_row + 1; is_zero && row < setup.nequations; row++) {
      if (first_var[row] == pivot_col) {
        selected_row = row;
        is_zero = false;
        break;
      }
    }

    if (is_zero)
      // there is no such row, next column!
      continue;

    if (pivot_row != selected_row) {
      std::iter_swap(setup.equations.begin() + pivot_row,
                     setup.equations.begin() + selected_row);
      std::iter_swap(first_var.begin() + pivot_row,
                     first_var.begin() + selected_row);
    }

    for (size_t i = pivot_row + 1; i < setup.nequations; i++) {
      if (setup.is_coeff_zero(i, pivot_col))
        continue;

      auto pivot_row_iter = std::begin(setup.equations[pivot_row]);
      auto curr_iter = std::begin(setup.equations[i]);
      std::vector<size_t> new_eq{};
      while (pivot_row_iter != std::end(setup.equations[pivot_row]) &&
             curr_iter != std::end(setup.equations[i])) {
        if (*pivot_row_iter == pivot_col) {
          pivot_row_iter++;
          continue;
        }

        if (*curr_iter == pivot_col) {
          curr_iter++;
          continue;
        }

        if (*pivot_row_iter < *curr_iter) {
          new_eq.emplace_back(*pivot_row_iter);
          pivot_row_iter++;
        } else if (*pivot_row_iter > *curr_iter) {
          new_eq.emplace_back(*curr_iter);
          curr_iter++;
        } else /* if (*pivot_row_iter == *curr_iter) */ {
          pivot_row_iter++;
          curr_iter++;
        }
      }

      while (pivot_row_iter != std::end(setup.equations[pivot_row])) {
        if (*pivot_row_iter == pivot_col) {
          pivot_row_iter++;
          continue;
        }

        new_eq.emplace_back(*pivot_row_iter);
        pivot_row_iter++;
      }

      while (curr_iter != std::end(setup.equations[i])) {
        if (*curr_iter == pivot_col) {
          curr_iter++;
          continue;
        }

        new_eq.emplace_back(*curr_iter);
        curr_iter++;
      }
      setup.equations[i] = new_eq;

      if (first_var[i] == pivot_col) {
        first_var[i] = sentinel;
        for (size_t j = 0; j < setup.nvars; j++) {
          if (!setup.is_coeff_zero(i, j)) {
            first_var[i] = j;
            break;
          }
        }
      }
    }

    pivot_row++;
  }

  for (auto idx = setup.nequations; idx-- > 0;) {
    if (first_var[idx] == sentinel) {
      if (setup.equations[idx].empty()) {
        // identity 0 = 0
        continue;
      } else if (setup.equations[idx][0] == setup.nvars) {
        return {SolutionKind::IMPOSSIBLE, {}};
      } else {
        error("UNREACHABLE");
      }
    }

    auto curr_var = first_var[idx];
    vars[curr_var] = setup.eval_equation_for_var(vars, idx, curr_var);
  }

  return {SolutionKind::SOLVED, vars};
}

template <>
typename mod_system<ModularSystemKind::MODN>::solution_type
gaussian_elimination(mod_system<ModularSystemKind::MODN> &setup) {
  std::vector<mpz_class> vars(setup.nvars);
  std::fill(vars.begin(), vars.end(), mpz_class{1});

  const size_t sentinel = std::numeric_limits<size_t>::max();
  debug_assert(sentinel > setup.nequations, "sentinel <= nequations");
  std::vector<size_t> first_var(setup.nequations);
  for (size_t idx = 0; idx < setup.equations.size(); idx++) {
    first_var[idx] = sentinel;
    for (size_t i = 0; i < setup.nvars; i++) {
      if (setup.equations[idx][i] != 0) {
        first_var[idx] = i;
        break;
      }
    }
  }

  size_t pivot_row = 0;
  mpz_class inv_pcoeff;
  for (size_t pivot_col = 0;
       pivot_col < setup.nvars && pivot_row < setup.nequations; pivot_col++) {

    size_t max_row = pivot_row;
    for (size_t row = pivot_row + 1; row < setup.nequations; row++) {
      auto &system = setup.equations;

      // std::abs(system[max_row][pivot_col]) < std::abs(system[row][pivot_col])
      if (mpz_cmpabs(system[max_row][pivot_col].get_mpz_t(),
                     system[row][pivot_col].get_mpz_t()) < 0)
        max_row = row;
    }

    if (setup.equations[max_row][pivot_col] == 0)
      continue;

    if (pivot_row != max_row) {
      std::iter_swap(setup.equations.begin() + pivot_row,
                     setup.equations.begin() + max_row);
      std::iter_swap(first_var.begin() + pivot_row,
                     first_var.begin() + max_row);
    }

    mpz_invert(inv_pcoeff.get_mpz_t(),
               setup.equations[pivot_row][pivot_col].get_mpz_t(),
               setup.modulo.get_mpz_t());
    for (size_t i = pivot_row + 1; i < setup.nequations; i++) {
      mpz_class k = (setup.equations[i][pivot_col] * inv_pcoeff);
      mpz_mod(k.get_mpz_t(), k.get_mpz_t(), setup.modulo.get_mpz_t());
      setup.equations[i][pivot_col] = 0;

      for (size_t j = pivot_col + 1; j < (setup.nvars + 1); j++) {
        mpz_class tmp = k * setup.equations[pivot_row][j];
        mpz_mod(tmp.get_mpz_t(), tmp.get_mpz_t(), setup.modulo.get_mpz_t());
        setup.equations[i][j] -= tmp;
        mpz_mod(setup.equations[i][j].get_mpz_t(),
                setup.equations[i][j].get_mpz_t(), setup.modulo.get_mpz_t());
      }

      if (first_var[i] == pivot_col) {
        first_var[i] = -1;
        for (size_t j = 0; j < setup.nvars; j++) {
          if (setup.equations[i][j] != 0) {
            first_var[i] = j;
            break;
          }
        }
      }
    }

    pivot_row++;
  }

  for (auto idx = setup.nequations; idx-- > 0;) {
    if (first_var[idx] == sentinel) {
      if (setup.equations[idx].back() != 0) {
        return {SolutionKind::IMPOSSIBLE, {}};
      } else {
        continue;
      }
    }

    auto curr_var = first_var[idx];
    vars[curr_var] = setup.eval_equation_for_var(vars, idx, curr_var);
  }

  return {SolutionKind::SOLVED, vars};
}

} // namespace ge

#endif
