/**
 * @file stopwatch.h
 * @author TheZxcv
 * @brief A stopwatch class for benchmarks
 */

#ifndef GE_STOPWATCH_H
#define GE_STOPWATCH_H

#include <chrono>
#include <ostream>

namespace bench {

class stop_watch {
  using chrono_clock = std::chrono::steady_clock;

private:
  bool is_running;
  chrono_clock::time_point start_time;
  chrono_clock::duration elapsed_time;
  std::string taskname;

private:
  chrono_clock::duration elapsed_duration() const;

public:
  stop_watch();
  ~stop_watch() = default;

  void start();
  void start(const std::string &);
  void stop();
  void reset();
  void restart();
  void restart(const std::string &);
  long elapsed() const;

  friend std::ostream &operator<<(std::ostream &, const stop_watch &);
};

} // namespace bench

#endif
