/**
 * @file indirect_priority_queue.h
 * @brief Indirect priority queue
 *
 * Shamelessly copied from
 * https://github.com/vigna/fastutil/blob/master/drv/HeapIndirectPriorityQueue.drv
 *
 * @author TheZxcv
 */

#ifndef LGE_INDIRECT_PRIORITY_QUEUE_H
#define LGE_INDIRECT_PRIORITY_QUEUE_H

#include "utils.h"

#include <algorithm>
#include <vector>

namespace queue {

template <typename T, typename ValueContainer,
          class Compare = std::less<typename ValueContainer::value_type>>
class indirect_priority_queue {
  using value_type = T;
  using index_type = std::size_t;
  using container_type = std::vector<index_type>;
  using reference = value_type &;
  using const_reference = const value_type &;
  using const_iterator = const index_type *;
  using size_type = std::size_t;
  constexpr static index_type sentinel_value =
      std::numeric_limits<index_type>::max();

public:
  indirect_priority_queue(const indirect_priority_queue &) = default;
  indirect_priority_queue(indirect_priority_queue &&) noexcept = default;
  indirect_priority_queue(const ValueContainer &container);

  indirect_priority_queue &operator=(const indirect_priority_queue &) = default;
  indirect_priority_queue &
  operator=(indirect_priority_queue &&) noexcept = default;

  ~indirect_priority_queue() = default;

  index_type top() const;
  bool empty() const;
  size_type size() const;

  index_type dequeue();
  void changed(index_type index);
  void all_changed();
  bool remove(index_type index);

  const_iterator begin() const;
  const_iterator cbegin() const;

  const_iterator end() const;
  const_iterator cend() const;

private:
  const Compare m_comp;
  const ValueContainer &m_reference;
  size_type m_size;
  container_type m_queue;
  container_type m_inversion;

  void make_heap(const ValueContainer &ref_array, container_type &heap,
                 container_type &inversion, size_type size);
  int down_heap(const ValueContainer &ref_array, container_type &heap,
                container_type &inversion, size_type size, index_type i);
  int up_heap(const ValueContainer &ref_array, container_type &heap,
              container_type &inversion, size_type size, index_type i);
};

template <typename T, typename ValueContainer, class Compare>
inline indirect_priority_queue<T, ValueContainer, Compare>::
    indirect_priority_queue(const ValueContainer &container)
    : m_comp{}, m_reference{container}, m_size{static_cast<size_type>(
                                            container.size())},
      m_queue(m_size), m_inversion(m_size) {

  static_assert(
      std::is_same<typename ValueContainer::value_type, value_type>::value,
      "The value_type must be the same.");

  index_type i = m_size;
  while (i-- != 0) {
    m_queue[i] = i;
    m_inversion[m_queue[i]] = i;
  }

  make_heap(m_reference, m_queue, m_inversion, m_size);
}

template <typename T, typename ValueContainer, class Compare>
inline typename indirect_priority_queue<T, ValueContainer, Compare>::index_type
indirect_priority_queue<T, ValueContainer, Compare>::top() const {
  return m_queue[0];
}

template <typename T, typename ValueContainer, class Compare>
inline bool indirect_priority_queue<T, ValueContainer, Compare>::empty() const {
  return m_size == 0;
}

template <typename T, typename ValueContainer, class Compare>
inline typename indirect_priority_queue<T, ValueContainer, Compare>::size_type
indirect_priority_queue<T, ValueContainer, Compare>::size() const {
  return m_size;
}

template <typename T, typename ValueContainer, class Compare>
inline typename indirect_priority_queue<T, ValueContainer, Compare>::index_type
indirect_priority_queue<T, ValueContainer, Compare>::dequeue() {
  debug_assert(m_size != 0, "queue empty");

  auto result = m_queue[0];
  if (--m_size != 0) {
    m_queue[0] = m_queue[m_size];
    m_inversion[m_queue[0]] = 0;
  }
  m_inversion[result] = sentinel_value;

  if (m_size != 0)
    down_heap(m_reference, m_queue, m_inversion, m_size, 0);
  return result;
}

template <typename T, typename ValueContainer, class Compare>
inline void
indirect_priority_queue<T, ValueContainer, Compare>::changed(index_type index) {
  const index_type pos = m_inversion[index];
  debug_assert(pos != sentinel_value,
               "Index `index` does not belong to the queue");
  const index_type new_pos =
      up_heap(m_reference, m_queue, m_inversion, m_size, pos);
  down_heap(m_reference, m_queue, m_inversion, m_size, new_pos);
}

template <typename T, typename ValueContainer, class Compare>
inline void indirect_priority_queue<T, ValueContainer, Compare>::all_changed() {
  make_heap(m_reference, m_queue, m_inversion, m_size);
}

template <typename T, typename ValueContainer, class Compare>
inline bool
indirect_priority_queue<T, ValueContainer, Compare>::remove(index_type index) {
  const auto result = m_inversion[index];
  if (result == sentinel_value)
    return false;
  m_inversion[index] = sentinel_value;

  if (result < --m_size) {
    m_queue[result] = m_queue[m_size];
    m_inversion[m_queue[result]] = result;

    const index_type new_pos =
        up_heap(m_reference, m_queue, m_inversion, m_size, result);
    down_heap(m_reference, m_queue, m_inversion, m_size, new_pos);
  }

  return true;
}

template <typename T, typename ValueContainer, class Compare>
inline
    typename indirect_priority_queue<T, ValueContainer, Compare>::const_iterator
    indirect_priority_queue<T, ValueContainer, Compare>::begin() const {
  return m_queue.data();
}

template <typename T, typename ValueContainer, class Compare>
inline
    typename indirect_priority_queue<T, ValueContainer, Compare>::const_iterator
    indirect_priority_queue<T, ValueContainer, Compare>::cbegin() const {
  return m_queue.data();
}

template <typename T, typename ValueContainer, class Compare>
inline
    typename indirect_priority_queue<T, ValueContainer, Compare>::const_iterator
    indirect_priority_queue<T, ValueContainer, Compare>::end() const {
  return m_queue.data() + m_size;
}

template <typename T, typename ValueContainer, class Compare>
inline
    typename indirect_priority_queue<T, ValueContainer, Compare>::const_iterator
    indirect_priority_queue<T, ValueContainer, Compare>::cend() const {
  return m_queue.data() + m_size;
}

template <typename T, typename ValueContainer, class Compare>
inline void indirect_priority_queue<T, ValueContainer, Compare>::make_heap(
    const ValueContainer &ref_array, container_type &heap,
    container_type &inversion, size_type size) {
  index_type i = size >> 1U;
  while (i-- != 0)
    down_heap(ref_array, heap, inversion, size, i);
}

template <typename T, typename ValueContainer, class Compare>
inline int indirect_priority_queue<T, ValueContainer, Compare>::down_heap(
    const ValueContainer &ref_array, container_type &heap,
    container_type &inversion, size_type size, size_type i) {
  debug_assert(i < size, "Index `i` out of bounds");

  const auto e = heap[i];
  const auto E = ref_array[e];
  index_type child;

  while ((child = (i << 1U) + 1U) < size) {
    auto t = heap[child];
    const auto right = child + 1;
    if (right < size && m_comp(ref_array[heap[right]], ref_array[t])) {
      t = heap[child = right];
    }
    if (E == ref_array[t] || m_comp(E, ref_array[t])) {
      break;
    }
    heap[i] = t;
    inversion[heap[i]] = i;
    i = child;
  }

  heap[i] = e;
  inversion[e] = i;
  return i;
}

template <typename T, typename ValueContainer, class Compare>
inline int indirect_priority_queue<T, ValueContainer, Compare>::up_heap(
    const ValueContainer &ref_array, container_type &heap,
    container_type &inversion, size_type size, size_type i) {
  debug_assert(i < size, "Index `i` out of bounds");
  (void)size;

  const auto e = heap[i];
  const auto E = ref_array[e];

  while (i != 0) {
    const size_type parent = (i - 1U) >> 1U;
    const auto t = heap[parent];
    if (ref_array[t] == E || m_comp(ref_array[t], E))
      break;
    heap[i] = t;
    inversion[heap[i]] = i;
    i = parent;
  }

  heap[i] = e;
  inversion[e] = i;

  return i;
}
} // namespace queue

#endif // LGE_INDIRECT_PRIORITY_QUEUE_H
