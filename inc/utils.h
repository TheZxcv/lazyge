/**
 * @file utils.h
 * @author TheZxcv
 * @brief Bunch of utilities
 */

#ifndef GE_UTILS_H
#define GE_UTILS_H

#include <algorithm>
#include <sstream>
#include <string>
#include <vector>

#ifdef _MSC_VER
#define FORCEINLINE __forceinline
#elif defined(__GNUC__)
#define FORCEINLINE inline __attribute__((__always_inline__))
#elif defined(__CLANG__)
#if __has_attribute(__always_inline__)
#define FORCEINLINE inline __attribute__((__always_inline__))
#else
#define FORCEINLINE inline
#endif
#else
#define FORCEINLINE inline
#endif

#if defined(__GNUC__) || defined(__CLANG__)
#define likely(x) __builtin_expect((x), 1)
#define unlikely(x) __builtin_expect((x), 0)
#else
#define likely(x) (x)
#define unlikely(x) (x)
#endif

/**
 * Prints an error message and terminate the execution.
 * @param message the error message
 **/
[[noreturn]] void error(const std::string &message);

/**
 * Prints an error message and the line where it occurred then terminate the
 *execution.
 * @param lineno the line
 * @param message the error message
 **/
[[noreturn]] void error_at_line(const int lineno, const std::string &message);

#ifdef NDEBUG
#define debug_assert(_cond, _message) ((void)0)
#else
void __debug_assert(bool condition, const std::string &message);
#define debug_assert(condition, message) __debug_assert(condition, message)
#define GE_DEBUG
#endif

/**
 * Turns an iterable container into a string separating each item by `delim`.
 * @param container the iterable container
 * @param delim delimiter that separates each item
 * @return the resulting string
 **/
template <typename T>
std::string to_str(const T &container, const std::string &delim = " ") {
  auto begin = std::cbegin(container);
  auto end = std::cend(container);
  std::stringstream ss;
  bool first = true;
  for (; begin != end; begin++) {
    if (!first)
      ss << "," << delim;
    ss << *begin;
    first = false;
  }
  return ss.str();
}

/**
 * De-deduplicate the given std::vector `vector`.
 * `vector` must be sorted.
 * @param vector
 * @return true if `vector` has any duplicate elements, otherwise false
 */
template <typename T> bool dedup(std::vector<T> &vector) {
  auto new_end = std::unique(std::begin(vector), std::end(vector));
  bool has_duplicates = new_end != std::end(vector);
  auto new_len =
      static_cast<size_t>(std::distance(std::begin(vector), new_end));
  vector.resize(new_len);

  return has_duplicates;
}

/**
 * Changes the file extensions on the given file path.
 * Examples:
 *  - `path/to/file.old_ext` => `path/to/file.new_ext`
 *  - `path/to/file` => `path/to/file.new_ext`
 *  - `path.to/file` => `path.to/file.new_ext`
 * @param filepath
 * @param new_ext the new file extension
 * @return the file path with the new file extension
 */
std::string change_extension(const std::string &filepath,
                             const std::string &new_ext);

class human_readable {
public:
  human_readable(size_t size) : value(size) {}
  std::ostream &operator()(std::ostream &) const;

private:
  size_t value;
};

std::ostream &operator<<(std::ostream &out, human_readable readable);

#endif
