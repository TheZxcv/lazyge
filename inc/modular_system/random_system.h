/**
 * @file random_system.h
 * @author TheZxcv
 * @brief utilities to generate a random system of equations.
 */

#ifndef LGE_RANDOM_SYSTEM_H
#define LGE_RANDOM_SYSTEM_H

#include "modular_system.h"
#include <random>

/**
 * Generates a random modular system of equations with `nequations` equations
 * and `nvars` variables modulo `modulo`.  The generated system has
 * exactly `k` * `nequations` non-zero coefficients and guarantees that
 * each equation has at least one non-zero coefficient.
 *
 * @param modulo the modulo
 * @param nequations number of equations
 * @param nvars number of variables
 * @param k the density of each equation
 * @param engine a pseudo random generator
 * @return the generated system of equations
 */
template <ModularSystemKind Kind>
mod_system<Kind> random_mod_system_by_factor(
    const typename mod_system<Kind>::modulo_type &modulo, size_t nequations,
    size_t nvars, unsigned int k, std::mt19937 &engine);

/**
 * Generates a random modular system of equations with `nequations` equations
 * and `nvars` variables modulo `modulo`.  Each equation has exactly
 * `var_count` non-zero coefficients.
 *
 * @param modulo the modulo
 * @param nequations number of equations
 * @param nvars number of variables
 * @param var_count the number of non-zero coefficients in each equation
 * @param engine a pseudo random generator
 * @return the generated system of equations
 */
template <ModularSystemKind Kind>
mod_system<Kind>
random_mod_system_by_count(const typename mod_system<Kind>::modulo_type &modulo,
                           size_t nequations, size_t nvars,
                           unsigned int var_count, std::mt19937 &engine);

template <>
mod_system<ModularSystemKind::MODN> random_mod_system_by_factor(
    const typename mod_system<ModularSystemKind::MODN>::modulo_type &modulo,
    size_t nequations, size_t nvars, unsigned int k, std::mt19937 &engine) {
  if (k > nvars) {
    error("factor too big");
  }
  std::vector<std::vector<mpz_class>> equations;
  equations.resize(nequations);
  for (auto &eq : equations) {
    eq.resize(nvars + 1);
  }

  std::uniform_int_distribution<size_t> random_equation(0, nequations - 1);
  std::uniform_int_distribution<size_t> random_variable(0, nvars - 1);
  std::uniform_int_distribution<long> uniform_no_zero(1, modulo.get_si() - 1);
  auto next_non_zero = [modulo, &uniform_no_zero, &engine]() {
    // do not use the PRNG if modulo is 2!
    // this is to get the same result as the random generator for sparse systems
    return modulo == 2 ? 1 : uniform_no_zero(engine);
  };
  std::uniform_int_distribution<long> uniform(0, modulo.get_si() - 1);

  for (auto i = 0U; i < nequations; i++) {
    size_t var_idx = random_variable(engine);
    equations[i][var_idx] = next_non_zero();
  }

  for (auto i = 0U; i < (k - 1) * nequations; i++) {
    size_t eq_idx, var_idx;
    do {
      eq_idx = random_equation(engine);
      var_idx = random_variable(engine);
    } while (equations[eq_idx][var_idx] != 0);

    equations[eq_idx][var_idx] = next_non_zero();
  }

  for (auto i = 0U; i < nequations; i++) {
    auto b = uniform(engine);
    equations[i].back() = b;
  }

  return {modulo, nvars, std::move(equations)};
}

template <>
mod_system<ModularSystemKind::MODN> random_mod_system_by_count(
    const typename mod_system<ModularSystemKind::MODN>::modulo_type &modulo,
    size_t nequations, size_t nvars, unsigned int var_count,
    std::mt19937 &engine) {
  if (var_count > nvars) {
    error("var_count too big");
  }
  std::vector<std::vector<mpz_class>> equations;
  equations.resize(nequations);
  for (auto &eq : equations) {
    eq.resize(nvars + 1);
  }

  std::uniform_int_distribution<size_t> random_index(0, nvars - 1);
  std::uniform_int_distribution<long> uniform(1, modulo.get_si() - 1);
  auto next_non_zero = [modulo, &uniform, &engine]() {
    // do not use the PRNG if modulo is 2!
    // this is to get the same result as the random generator for sparse systems
    return modulo == 2 ? 1 : uniform(engine);
  };
  for (auto &eq : equations) {
    if (random_index(engine) == 0) {
      auto b = next_non_zero();
      eq.back() = b;
    }

    for (auto i = 0U; i < var_count; i++) {
      size_t idx;
      do {
        idx = random_index(engine);
      } while (eq[idx] != 0);

      eq[idx] = next_non_zero();
    }
  }

  return {modulo, nvars, std::move(equations)};
}

template <>
mod_system<ModularSystemKind::MOD2> random_mod_system_by_factor(
    const typename mod_system<ModularSystemKind::MOD2>::modulo_type &modulo,
    size_t nequations, size_t nvars, unsigned int k, std::mt19937 &engine) {
  if (k > nvars) {
    error("factor too big");
  }
  std::vector<std::vector<size_t>> equations(static_cast<size_t>(nequations));

  std::uniform_int_distribution<size_t> random_equation(0, nequations - 1);
  std::uniform_int_distribution<size_t> random_variable(0, nvars - 1);
  std::uniform_int_distribution<long> uniform(0, modulo - 1);

  for (auto i = 0U; i < nequations; i++) {
    size_t var_idx = random_variable(engine);
    equations[i].push_back(var_idx);
  }

  for (auto i = 0U; i < (k - 1) * nequations; i++) {
    size_t eq_idx, var_idx;
    std::vector<size_t>::iterator begin, end;
    do {
      eq_idx = random_equation(engine);
      var_idx = random_variable(engine);

      // early exit
      if (equations[eq_idx].empty() || equations[eq_idx][0] > var_idx)
        break;

      begin = std::begin(equations[eq_idx]);
      end = std::end(equations[eq_idx]);
    } while (std::binary_search(begin, end, var_idx));

    // ordered insertion
    auto pos =
        std::upper_bound(equations[eq_idx].begin(), equations[eq_idx].end(),
                         var_idx, std::less<>());
    equations[eq_idx].insert(pos, var_idx);
  }

  for (auto i = 0U; i < nequations; i++) {
    debug_assert(!equations[i].empty(), "equation empty!");
    if (uniform(engine) != 0) {
      equations[i].push_back(nvars);
    }
  }

  return {modulo, nvars, std::move(equations)};
}

template <>
mod_system<ModularSystemKind::MOD2> random_mod_system_by_count(
    const typename mod_system<ModularSystemKind::MOD2>::modulo_type &modulo,
    size_t nequations, size_t nvars, unsigned int var_count,
    std::mt19937 &engine) {
  if (var_count > nvars) {
    error("var_count too big");
  }
  std::vector<std::vector<size_t>> equations(static_cast<size_t>(nequations));

  std::uniform_int_distribution<size_t> random_index(0, nvars - 1);
  for (auto &eq : equations) {
    if (random_index(engine) == 0) {
      eq.push_back(nvars);
    }

    for (auto i = 0U; i < var_count; i++) {
      size_t idx;
      do {
        idx = random_index(engine);
      } while (std::find(std::begin(eq), std::end(eq), idx) != std::end(eq));
      eq.push_back(idx);
    }

    std::sort(eq.begin(), eq.end(), std::less<>());
  }

  return {modulo, nvars, std::move(equations)};
}

#endif
