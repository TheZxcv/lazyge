/**
 * @file gfp.h
 * @author TheZxcv
 * @brief operations on modular system of equations on GF(p)
 */

#ifndef LGE_GFp_H
#define LGE_GFp_H

#include "base_modular_system.h"

#include "utils.h"

template <>
bool mod_system<ModularSystemKind::MODN>::check_solution(
    const solution_type &solution) const {
  if (solution.vars.size() != static_cast<size_t>(nvars))
    return false;

  for (auto &eq : equations) {
    mpz_class acc = 0;
    for (size_t i = 0; i < solution.vars.size(); i++) {
      mpz_class product = (solution.vars[i] * eq[i]) % modulo;
      acc = (acc + product) % modulo;
    }
    if (eq.back() != acc)
      return false;
  }

  return true;
}

template <> mod_system<ModularSystemKind::MODN> read_system(std::istream &is) {
  mpz_class modulo;
  int nvars = 0;
  int nequations = 0;
  std::vector<std::vector<mpz_class>> equations{};

  bool read_header = false;
  int read_equations = 0;
  char c;
  int lineno = 1;
  while ((void)(c = static_cast<char>(is.get())), is.good()) {
    switch (c) {
      /* comment */
    case 'c':
      is.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
      lineno++;
      break;

      /* header */
    case 'h': {
      if (read_header) {
        error_at_line(lineno, "duplicated header.");
      }
      if (!(is >> modulo)) {
        error_at_line(lineno, "could not read modulo from header.");
      } else if (modulo < 1) {
        error_at_line(lineno, "modulo must be greater than 1.");
      }
      if (!(is >> nvars)) {
        error_at_line(lineno,
                      "could not read number of variables from header.");
      } else if (nvars < 0) {
        error_at_line(lineno, "the number of variables must be positive.");
      }
      if (!(is >> nequations)) {
        error_at_line(lineno,
                      "could not read number of equations from header.");
      } else if (nequations < 0) {
        error_at_line(lineno, "the number of equations must be positive.");
      }
      is.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
      lineno++;
      read_header = true;

      break;
    }

      /* equation */
    case 'e': {
      read_equations++;
      if (!read_header) {
        error_at_line(lineno, "encountered equation before header.");
      }

      if (read_equations > nequations) {
        error_at_line(lineno, "encountered more equations than expected.");
      }

      std::vector<mpz_class> equation{};
      std::string line{};
      if (!std::getline(is, line)) {
        error_at_line(lineno, "failed to read equation.");
      }

      std::istringstream iss{line};
      for (int i = 0; i < nvars + 1; i++) {
        int coeff;
        if (iss >> coeff) {
          if (coeff >= modulo) {
            std::stringstream ss{};
            ss << "invalid coefficient value: `" << coeff << "`.";
            error_at_line(lineno, ss.str());
          }
          equation.emplace_back(coeff);
        } else if (iss.eof()) {
          error_at_line(lineno, "failed to read equation.");
        } else {
          error_at_line(lineno, "encountered unexpected character.");
        }
      }

      if (!(iss >> std::ws).eof()) {
        // report error if there is any trailing non-blank character
        error_at_line(lineno,
                      "encountered unexpected character after equation.");
      }
      equations.emplace_back(std::move(equation));
      lineno++;
      break;
    }

      /* blank line */
    case '\n':
      break;

    default:
      if (!std::isblank(c)) {
        std::stringstream ss{};
        ss << "encountered invalid character `" << c
           << "` while parsing header.";
        error_at_line(lineno, ss.str());
      }
      break;
    }
  }

  if (!read_header) {
    error("no header.");
  }

  if (static_cast<size_t>(nequations) > equations.size()) {
    error("read less equations than expected.");
  }

  return {modulo, static_cast<size_t>(nvars), equations};
}

template <>
void write_system(std::ostream &os,
                  const mod_system<ModularSystemKind::MODN> &system) {
  os << "c modulo #variables #equations" << std::endl
     << "h " << system.modulo << " " << system.nvars << " " << system.nequations
     << std::endl;
  for (const auto &eq : system.equations) {
    os << "e";
    for (auto &c : eq) {
      os << " " << c;
    }
    os << std::endl;
  }
}

/**
 * Calculates (`a` - (`b` * `c`)) (mod `mod`).
 * @param a
 * @param b
 * @param c
 * @param mod
 * @return the result
 */
static inline mpz_class a_minus_b_times_c(const mpz_class &a,
                                          const mpz_class &b,
                                          const mpz_class &c,
                                          const mpz_class &mod) {
  mpz_class tmp = b * c;
  mpz_mod(tmp.get_mpz_t(), tmp.get_mpz_t(), mod.get_mpz_t());
  tmp = a - tmp;
  mpz_mod(tmp.get_mpz_t(), tmp.get_mpz_t(), mod.get_mpz_t());
  return tmp;
}

template <>
typename mod_system<ModularSystemKind::MODN>::peel_candidates_type
mod_system<ModularSystemKind::MODN>::substitute(const size_t pivot_idx,
                                                const size_t pivot_eq_idx) {
  std::vector<size_t> var_candidates{};
  mpz_t inv_pcoeff, tmp;
  mpz_class k;
  mpz_init(inv_pcoeff);
  mpz_init(tmp);

  mpz_invert(inv_pcoeff, equations[pivot_eq_idx][pivot_idx].get_mpz_t(),
             modulo.get_mpz_t());
  auto &pivot_eq = equations[pivot_eq_idx];
  for (auto &eq : equations) {
    size_t eq_idx = &eq - &equations[0];
    if (likely(ignored_eq[eq_idx] || eq[pivot_idx] == 0 ||
               eq_idx == pivot_eq_idx))
      continue;

    mpz_mul(k.get_mpz_t(), eq[pivot_idx].get_mpz_t(), inv_pcoeff);
    mpz_mod(k.get_mpz_t(), k.get_mpz_t(), modulo.get_mpz_t());

    debug_assert(eq2var_cnt[eq_idx] != 0, "Negative eq2var count!");
    debug_assert(var2eq_cnt[pivot_idx] != 0, "Negative var2eq count!");
    eq[pivot_idx] = 0;
    eq2var_cnt[eq_idx]--;
    var2eq_cnt[pivot_idx]--;
    var2xored_eqs[pivot_idx] ^= eq_idx;
    // the pivot variable becomes solved after a substitution
    // therefore we do not consider it as a peeling candidate

    eq_states[eq_idx].prio--;
    if (eq_states[eq_idx].state == SPARSE) {
      if (eq_states[eq_idx].prio == 0) {
        zero_or_one_prio.push_front(eq_idx);
      } else if (eq_states[eq_idx].prio == 1) {
        zero_or_one_prio.push_back(eq_idx);
      }
    }

    for (size_t i = 0; i < nvars; i++) {
      if (i == pivot_idx)
        continue;

      bool was_zero = eq[i] == 0;
      eq[i] = a_minus_b_times_c(eq[i], k, pivot_eq[i], modulo);
      bool is_now_zero = eq[i] == 0;

      if (was_zero && !is_now_zero) {
        var2eq_cnt[i]++;
        var2xored_eqs[i] ^= eq_idx;
        eq2var_cnt[eq_idx]++;
      } else if (!was_zero && is_now_zero) {
        var2eq_cnt[i]--;
        var2xored_eqs[i] ^= eq_idx;
        eq2var_cnt[eq_idx]--;
      }

      if (var2eq_cnt[i] == 1 &&
          (var_states[i].state == ACTIVE || var_states[i].state == IDLE)) {
        var_candidates.push_back(i);
      }
    }

    // constant term
    eq.back() = a_minus_b_times_c(eq.back(), k, pivot_eq.back(), modulo);
  }

  mpz_clear(inv_pcoeff);
  mpz_clear(tmp);

  // Dedup candidate list
  std::sort(var_candidates.begin(), var_candidates.end());
  dedup(var_candidates);

  return var_candidates;
}

template <>
typename mod_system<ModularSystemKind::MODN>::peel_candidates_type
mod_system<ModularSystemKind::MODN>::ignore_equation(size_t eq_idx) {
  debug_assert(eq_idx < equations.size(), "eq_idx out of bounds");
  std::vector<size_t> var_candidates{};

  for (size_t var_idx = 0; var_idx < nvars; var_idx++) {
    if (equations[eq_idx][var_idx] != 0) {
      var2eq_cnt[var_idx]--;
      var2xored_eqs[var_idx] ^= eq_idx;

      if (var2eq_cnt[var_idx] == 1 && (var_states[var_idx].state == ACTIVE ||
                                       var_states[var_idx].state == IDLE)) {
        var_candidates.push_back(var_idx);
      }
    }
  }
  ignored_eq[eq_idx] = true;

  // Dedup is not needed as only one equation is considered
  return var_candidates;
}

template <ModularSystemKind Kind> void mod_system<Kind>::init() {
  debug_assert(equations.size() == nequations,
               "nequations is not equal to equations.size()");
  if (nequations > 0) {
    debug_assert(equations[0].size() - 1 == nvars,
                 "nvars is not equal to equations[0].size() - 1");
  }

  std::fill(var2eq_cnt.begin(), var2eq_cnt.end(), 0);
  std::fill(var2xored_eqs.begin(), var2xored_eqs.end(), 0);

  std::fill(ignored_eq.begin(), ignored_eq.end(), false);

  for (size_t eq_idx = 0; eq_idx < static_cast<size_t>(nequations); eq_idx++) {
    eq2var_cnt[eq_idx] = 0;
    for (size_t var_idx = 0; var_idx < static_cast<size_t>(nvars); var_idx++) {
      if (equations[eq_idx][var_idx] != 0) {
        var2eq_cnt[var_idx]++;
        var2xored_eqs[var_idx] ^= eq_idx;
        eq2var_cnt[eq_idx]++;
      }
    }
  }

  {
    size_t idx = 0;
    for (auto &v : var_states) {
      v.idx = idx++;
      v.state = IDLE;
    }
  }

  eq_states.reserve(equations.size());
  for (size_t i = 0; i < equations.size(); i++) {
    eq_states.emplace_back(eq2var_cnt[i], SPARSE);

    if (eq2var_cnt[i] == 0) {
      zero_or_one_prio.push_front(i);
    } else if (eq2var_cnt[i] == 1) {
      zero_or_one_prio.push_back(i);
    }
  }
}

template <>
typename mod_system<ModularSystemKind::MODN>::value_type
mod_system<ModularSystemKind::MODN>::eval_equation_for_var(
    const std::vector<value_type> &solution, size_t eq_idx,
    size_t var_idx) const {
  mpz_class tmp;
  auto acc = equations[eq_idx].back();

  for (size_t i = 0; i < nvars; i++) {
    if (i == var_idx) {
      continue;
    }

    acc = a_minus_b_times_c(acc, solution[i], equations[eq_idx][i], modulo);
  }

  const auto mod = modulo.get_mpz_t();
  mpz_invert(tmp.get_mpz_t(), equations[eq_idx][var_idx].get_mpz_t(), mod);
  tmp = tmp * acc;
  mpz_mod(tmp.get_mpz_t(), tmp.get_mpz_t(), mod);
  return tmp;
}

template <>
bool FORCEINLINE mod_system<ModularSystemKind::MODN>::is_coeff_zero(
    size_t eq_idx, size_t var_idx) const {
  return equations[eq_idx][var_idx] == 0;
}

template <>
mpz_class FORCEINLINE
mod_system<ModularSystemKind::MODN>::constant_term(size_t eq_idx) const {
  return equations[eq_idx][nvars];
}

template <>
mod_system<ModularSystemKind::MODN>
mod_system<ModularSystemKind::MODN>::build_dense_system(int active_vars) const {
  std::vector<std::vector<mpz_class>> dense_equations{};
  for (size_t i = 0; i < nequations; i++) {
    const auto &eq = eq_states[i];
    if (eq.state != DENSE || ignored_eq[i])
      continue;

    std::vector<mpz_class> equation{};
    equation.reserve(static_cast<size_t>(active_vars) + 1);
    for (size_t idx = 0; idx < nvars; idx++) {
      auto &var = var_states[idx];
      if (var.state == ACTIVE) {
        equation.emplace_back(equations[i][idx]);
      }
    }
    equation.emplace_back(equations[i].back());
    dense_equations.emplace_back(equation);
  }
  return {modulo, static_cast<size_t>(active_vars), std::move(dense_equations)};
}

#endif
