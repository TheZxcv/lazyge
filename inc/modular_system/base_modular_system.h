/**
 * @file base_modular_system.h
 * @author TheZxcv
 * @brief definition of operations on modular system of equations
 */

#ifndef LGE_BASE_MODULAR_SYSTEM_H
#define LGE_BASE_MODULAR_SYSTEM_H

#include "utils.h"

#include <deque>
#include <gmpxx.h>
#include <istream>
#include <sstream>
#include <tuple>
#include <vector>

enum VarState { ACTIVE, IDLE, SOLVED, PEELED };
constexpr const char *VARIABLE_STATES[] = {"active", "idle", "solved",
                                           "peeled"};
enum EquationState { SPARSE, DENSE, IGNORED };
constexpr const char *EQUATION_STATES[] = {"sparse", "dense", "ignored"};

struct equation_state {
  int prio;
  EquationState state;

  equation_state(int priority, EquationState eq_state)
      : prio(priority), state(eq_state) {}

  friend std::ostream &operator<<(std::ostream &stream,
                                  const equation_state &eq) {
    return stream << "eq[p: " << eq.prio << ", " << EQUATION_STATES[eq.state]
                  << "]";
  }
};

struct variable_state {
  size_t idx;
  VarState state;

  friend std::ostream &operator<<(std::ostream &stream,
                                  const variable_state &v) {
    return stream << "v" << v.idx << "[" << VARIABLE_STATES[v.state] << "]";
  }
};

enum class SolutionKind { SOLVED, IMPOSSIBLE };
enum class ModularSystemKind { MOD2, MODN };

template <ModularSystemKind Kind> class mod_system {
public:
  typedef typename std::conditional<(Kind == ModularSystemKind::MOD2), size_t,
                                    mpz_class>::type element_type;
  typedef typename std::conditional<(Kind == ModularSystemKind::MOD2), bool,
                                    mpz_class>::type value_type;
  typedef typename std::conditional<(Kind == ModularSystemKind::MOD2),
                                    unsigned long, mpz_class>::type modulo_type;
  struct solution_type {
    SolutionKind kind;
    std::vector<value_type> vars;
  };

  using peel_candidates_type = std::vector<size_t>;

private:
  /**
   * Initialises the system
   */
  void init();

public:
  const size_t nvars;
  const size_t nequations;
  const modulo_type modulo;
  std::vector<std::vector<element_type>> equations;

  std::vector<size_t> var2eq_cnt;
  std::vector<size_t> var2xored_eqs;
  std::vector<size_t> eq2var_cnt;
  std::vector<bool> ignored_eq;

  // queue of equations with priority 0 or 1
  std::deque<size_t> zero_or_one_prio;
  // stack of solved and peeled <variable, equation>
  std::vector<std::tuple<size_t, size_t>> stack_of_solved;

  // states
  std::vector<equation_state> eq_states;
  std::vector<variable_state> var_states;

  mod_system() = delete;
  mod_system &operator=(const mod_system &) = delete;
  mod_system(const mod_system &other)
      : nvars(other.nvars), nequations(other.nequations), modulo(other.modulo),
        var2eq_cnt(other.nvars), var2xored_eqs(other.nvars),
        eq2var_cnt(other.nequations), ignored_eq(other.nequations),
        zero_or_one_prio(other.zero_or_one_prio),
        stack_of_solved(other.stack_of_solved), eq_states(), var_states(nvars) {
    equations.reserve(other.equations.size());
    for (auto &eq : other.equations) {
      std::vector<element_type> coeffs;
      coeffs.reserve(eq.size());
      for (const auto &c : eq) {
        coeffs.emplace_back(c);
      }
      equations.emplace_back(std::move(coeffs));
    }
    init();
  }

  mod_system(modulo_type modulo_, size_t nvars_,
             std::vector<std::vector<element_type>> equations_)
      : nvars(nvars_), nequations(equations_.size()),
        modulo(std::move(modulo_)), equations(std::move(equations_)),
        var2eq_cnt(), var2xored_eqs(), eq2var_cnt(), ignored_eq(),
        zero_or_one_prio(), stack_of_solved(), eq_states(), var_states() {
    var2eq_cnt.resize(nvars);
    var2xored_eqs.resize(nvars);
    eq2var_cnt.resize(equations.size());
    ignored_eq.resize(equations.size());
    var_states.resize(nvars);
    init();
  }

  /**
   * Eliminates variable `pivot_idx` on all equation in the system using the
   * equation `pivot_eq_idx`.
   * @param pivot_idx the pivot variable
   * @param pivot_eq_idx the pivot equation's index
   * @return peeling candidates
   */
  peel_candidates_type substitute(size_t pivot_idx, size_t pivot_eq_idx);

  /**
   * Evaluates the equation `eq_idx` for the variable `var_idx` using
   * the values provided in the partial solution `solution` for the other
   * variables.
   * @param solution partial solution
   * @param eq_idx the equation's index
   * @param var_idx the variable's index
   * @return the value for the variable `var_idx`
   */
  value_type eval_equation_for_var(const std::vector<value_type> &solution,
                                   size_t eq_idx, size_t var_idx) const;

  /**
   * Returns whether or not the variable `var_idx` has coefficient 0
   * in the equation `eq_idx`.
   * @param eq_idx the equation's index
   * @param var_idx the variable's index
   * @return true if `var_idx` has coefficient 0 in `eq_idx`, otherwise false
   */
  bool is_coeff_zero(size_t eq_idx, size_t var_idx) const;

  /**
   * Returns the value of the constant term for the equations `eq_idx`.
   * @param eq_idx the equation's index
   * @return the value of the constant term
   */
  value_type constant_term(size_t eq_idx) const;

  /**
   * Flags equation `eq_idx` as ignored and updates support counters.
   * @param eq_idx the equation's index
   * @return peeling candidates
   */
  peel_candidates_type ignore_equation(size_t eq_idx);

  /**
   * Checks that the given solution `solution` satisfies the
   * system of equations.
   * @param sol solution to the system
   * @return true if the solution satisfies the system, otherwise false
   */
  bool check_solution(const solution_type &solution) const;

  /**
   * Constructs a new `mod_system` containing only active variables
   * and dense equations.
   * @param active_vars the number of active variables
   * @return the dense modular system
   */
  mod_system<Kind> build_dense_system(int active_vars) const;

  /**
   * Generates a description for the modular system.
   * @return a string containing the description
   */
  std::string summary() const;
};

/**
 * Reads a system of equations from the input stream `is`.
 * @param is the input stream
 * @return the read system of equations
 */
template <ModularSystemKind kind>
mod_system<kind> read_system(std::istream &is);

/**
 * Writes a system of equations to the output stream `os`.
 * @param os the output stream
 * @param system the system of equations
 */
template <ModularSystemKind kind>
void write_system(std::ostream &os, const mod_system<kind> &system);

template <ModularSystemKind Kind>
std::string mod_system<Kind>::summary() const {
  size_t equation_capacity = equations.empty() ? 0 : equations[0].capacity();
  size_t estimated_size =
      sizeof(std::vector<element_type>) * equations.capacity() +
      sizeof(element_type) * equation_capacity * equations.capacity();

  size_t nonzeroes = 0;
  for (size_t i = 0; i < equations.size(); i++) {
    nonzeroes += eq2var_cnt[i];
  }

  double density;
  if (nvars != 0 && nequations != 0) {
    density = nonzeroes / static_cast<double>(nvars * nequations);
  } else {
    density = 1;
  }

  std::ostringstream oss{};
  oss << "---Summary---" << std::endl
      << "   modulo: " << modulo << std::endl
      << "variables: " << nvars << std::endl
      << "equations: " << nequations << std::endl
      << "  density: " << density << std::endl
      << "est. size: " << human_readable(estimated_size) << std::endl
      << "-------------" << std::endl;
  return oss.str();
}

#endif
