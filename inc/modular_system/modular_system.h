/**
 * @file modular_system.h
 * @author TheZxcv
 * @brief operations on modular system of equations
 */

#ifndef LGE_MODULAR_SYSTEM_H
#define LGE_MODULAR_SYSTEM_H

#include "base_modular_system.h"
#include "gf2.h"
#include "gfp.h"

#endif
