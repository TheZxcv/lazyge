/**
 * @file gf2.h
 * @author TheZxcv
 * @brief operations on modular system of equations on GF(2)
 */

#ifndef LGE_GF2_H
#define LGE_GF2_H

#include "base_modular_system.h"

#include "utils.h"

#include <iostream>

template <>
bool mod_system<ModularSystemKind::MOD2>::check_solution(
    const solution_type &sol) const {
  if (sol.vars.size() != static_cast<size_t>(nvars))
    return false;

  for (auto &eq : equations) {
    unsigned acc = 0;
    for (auto i : eq) {
      if (i == nvars || sol.vars[i])
        acc ^= 1U;
    }
    if (acc != 0)
      return false;
  }

  return true;
}

template <> mod_system<ModularSystemKind::MOD2> read_system(std::istream &is) {
  unsigned int modulo = 0;
  int nvars = 0;
  int nequations = 0;
  std::vector<std::vector<size_t>> equations{};

  bool read_header = false;
  char c;
  int lineno = 1;
  while ((void)(c = static_cast<char>(is.get())), is.good()) {
    switch (c) {
      /* comment */
    case 'c':
      is.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
      lineno++;
      break;

      /* header */
    case 'h': {
      if (read_header) {
        error_at_line(lineno, "duplicated header.");
      }

      std::string type;
      is >> type;
      if (type != "s") {
        std::stringstream ss{};
        ss << "unexpected type `" << type << "`.";
        error_at_line(lineno, ss.str());
      }
      if (!(is >> modulo)) {
        error_at_line(lineno, "could not read modulo from header.");
      } else if (modulo != 2) {
        error_at_line(lineno,
                      "only GF(2) systems are supported in sparse mode.");
      }
      if (!(is >> nvars)) {
        error_at_line(lineno,
                      "could not read number of variables from header.");
      } else if (nvars < 0) {
        error_at_line(lineno, "the number of variables must be positive.");
      }
      if (!(is >> nequations)) {
        error_at_line(lineno,
                      "could not read number of equations from header.");
      } else if (nequations < 0) {
        error_at_line(lineno, "the number of equations must be positive.");
      }
      equations.resize(nequations);
      is.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
      lineno++;
      read_header = true;

      break;
    }

      /* equation */
    case 'e': {
      if (!read_header) {
        error_at_line(lineno, "encountered equation before header.");
      }

      std::string line{};
      if (!std::getline(is, line)) {
        error_at_line(lineno, "failed to read equation.");
      }

      std::istringstream iss{line};

      size_t equation_idx;
      if (!(iss >> equation_idx)) {
        error_at_line(lineno, "failed to read equation.");
      }

      if (equation_idx >= static_cast<size_t>(nequations)) {
        std::stringstream ss{};
        ss << "equation index out of bound: " << equation_idx << ".";
        error_at_line(lineno, ss.str());
      }

      std::string colon;
      iss >> colon;
      if (colon != ":") {
        std::stringstream ss{};
        ss << "expected ':', found `" << colon << "`.";
        error_at_line(lineno, ss.str());
      }

      size_t idx;
      while (iss >> idx) {
        if (idx < static_cast<size_t>(nvars) + 1U) {
          equations[equation_idx].emplace_back(idx);
        } else {
          std::stringstream ss{};
          ss << "variable index out of bound: " << idx << ".";
          error_at_line(lineno, ss.str());
        }
      }

      std::sort(equations[equation_idx].begin(), equations[equation_idx].end(),
                std::less<>());
      if (dedup(equations[equation_idx])) {
        std::cerr << "[" << lineno << "]: duplicate entry(ies) in equation "
                  << equation_idx << "." << std::endl;
      }

      if (!(iss >> std::ws).eof()) {
        // report error if there is any trailing non-blank character
        error_at_line(lineno,
                      "encountered unexpected character after equation.");
      }
      lineno++;
      break;
    }

      /* blank line */
    case '\n':
      lineno++;
      break;

    default:
      if (!std::isblank(c)) {
        std::stringstream ss{};
        ss << "encountered invalid character `" << c
           << "` while parsing header.";
        error_at_line(lineno, ss.str());
      }
      break;
    }
  }

  if (!read_header) {
    error("no header.");
  }

  return {modulo, static_cast<size_t>(nvars), equations};
}

template <>
void write_system(std::ostream &os,
                  const mod_system<ModularSystemKind::MOD2> &system) {
  os << "c modulo #variables #equations" << std::endl
     << "h s " << system.modulo << " " << system.nvars << " "
     << system.nequations << std::endl;
  int i = 0;
  for (const auto &eq : system.equations) {
    os << "e " << i++ << ": ";
    for (auto &c : eq) {
      os << " " << c;
    }
    os << std::endl;
  }
}

template <>
bool FORCEINLINE mod_system<ModularSystemKind::MOD2>::is_coeff_zero(
    size_t eq_idx, size_t var_idx) const {
  auto begin = std::begin(equations[eq_idx]);
  auto end = std::end(equations[eq_idx]);
  return !std::binary_search(begin, end, var_idx);
}

template <>
bool FORCEINLINE
mod_system<ModularSystemKind::MOD2>::constant_term(size_t eq_idx) const {
  return !equations[eq_idx].empty() && equations[eq_idx].back() == nvars;
}

template <>
typename mod_system<ModularSystemKind::MOD2>::peel_candidates_type
mod_system<ModularSystemKind::MOD2>::substitute(const size_t pivot_idx,
                                                const size_t pivot_eq_idx) {
  std::vector<size_t> var_candidates{};
  auto &pivot_eq = equations[pivot_eq_idx];
  for (auto &eq : equations) {
    // early exit
    if (var2eq_cnt[pivot_idx] == 0)
      break;

    size_t eq_idx = &eq - &equations[0];
    if (likely(eq_idx == pivot_eq_idx || ignored_eq[eq_idx] ||
               is_coeff_zero(eq_idx, pivot_idx)))
      continue;

    debug_assert(eq2var_cnt[eq_idx] != 0, "Negative eq2var count!");
    debug_assert(var2eq_cnt[pivot_idx] != 0, "Negative var2eq count!");
    // *virtually* remove pivot_idx from eq (eq[pivot_idx] = 0)
    eq2var_cnt[eq_idx]--;
    var2eq_cnt[pivot_idx]--;
    var2xored_eqs[pivot_idx] ^= eq_idx;
    // the pivot variable becomes solved after a substitution
    // therefore we do not consider it as a peeling candidate

    eq_states[eq_idx].prio--;
    if (eq_states[eq_idx].state == SPARSE) {
      if (eq_states[eq_idx].prio == 0) {
        zero_or_one_prio.push_front(eq_idx);
      } else if (eq_states[eq_idx].prio == 1) {
        zero_or_one_prio.push_back(eq_idx);
      }
    }

    // symmetric difference
    auto pivot_eq_iter = std::begin(pivot_eq);
    auto eq_iter = std::begin(eq);
    std::vector<size_t> new_eq{};
    while (pivot_eq_iter != std::end(pivot_eq) && eq_iter != std::end(eq)) {
      if (*pivot_eq_iter == pivot_idx) {
        pivot_eq_iter++;
        continue;
      }

      if (*eq_iter == pivot_idx) {
        eq_iter++;
        continue;
      }

      size_t i;
      if (*pivot_eq_iter < *eq_iter) {
        i = *pivot_eq_iter;
        if (i < nvars) {
          var2eq_cnt[i]++;
          var2xored_eqs[i] ^= eq_idx;
          eq2var_cnt[eq_idx]++;
        }
        new_eq.emplace_back(i);
        pivot_eq_iter++;
      } else if (*pivot_eq_iter > *eq_iter) {
        new_eq.emplace_back(*eq_iter);
        // No changes to the variable's counter
        eq_iter++;
        continue;
      } else /* if (*pivot_eq_iter == *eq_iter) */ {
        i = *eq_iter;
        if (i < nvars) {
          var2eq_cnt[i]--;
          var2xored_eqs[i] ^= eq_idx;
          eq2var_cnt[eq_idx]--;
        }
        pivot_eq_iter++;
        eq_iter++;
      }

      if (i < nvars && var2eq_cnt[i] == 1 &&
          (var_states[i].state == ACTIVE || var_states[i].state == IDLE)) {
        var_candidates.push_back(i);
      }
    }

    while (pivot_eq_iter != std::end(pivot_eq)) {
      if (*pivot_eq_iter == pivot_idx) {
        pivot_eq_iter++;
        continue;
      }

      size_t i = *pivot_eq_iter;
      new_eq.emplace_back(*pivot_eq_iter);
      pivot_eq_iter++;

      if (i < nvars) {
        var2eq_cnt[i]++;
        var2xored_eqs[i] ^= eq_idx;
        eq2var_cnt[eq_idx]++;
      }
    }

    while (eq_iter != std::end(eq)) {
      if (*eq_iter == pivot_idx) {
        eq_iter++;
        continue;
      }
      new_eq.emplace_back(*eq_iter);
      eq_iter++;
    }

    equations[eq_idx] = new_eq;
  }

  // Dedup candidate list
  std::sort(var_candidates.begin(), var_candidates.end());
  dedup(var_candidates);

  return var_candidates;
}

template <>
typename mod_system<ModularSystemKind::MOD2>::peel_candidates_type
mod_system<ModularSystemKind::MOD2>::ignore_equation(size_t eq_idx) {
  debug_assert(eq_idx < equations.size(), "eq_idx out of bounds");
  std::vector<size_t> var_candidates{};

  for (auto var_idx : equations[eq_idx]) {
    if (var_idx == nvars)
      continue;
    var2eq_cnt[var_idx]--;
    var2xored_eqs[var_idx] ^= eq_idx;

    if (var2eq_cnt[var_idx] == 1 && (var_states[var_idx].state == ACTIVE ||
                                     var_states[var_idx].state == IDLE)) {
      var_candidates.push_back(var_idx);
    }
  }
  ignored_eq[eq_idx] = true;

  // Dedup is not needed as only one equation is considered
  return var_candidates;
}

template <> void mod_system<ModularSystemKind::MOD2>::init() {
  debug_assert(modulo == 2, "modulo != 2");
  debug_assert(equations.size() == nequations,
               "nequations is not equal to equations.size()");

  var2eq_cnt.resize(nvars);
  std::fill(var2eq_cnt.begin(), var2eq_cnt.end(), 0);
  var2xored_eqs.resize(nvars);
  std::fill(var2xored_eqs.begin(), var2xored_eqs.end(), 0);

  eq2var_cnt.resize(nequations);
  ignored_eq.resize(nequations);
  std::fill(ignored_eq.begin(), ignored_eq.end(), false);

  for (size_t eq_idx = 0; eq_idx < static_cast<size_t>(nequations); eq_idx++) {
    eq2var_cnt[eq_idx] = 0;

    for (auto var_idx : equations[eq_idx]) {
      if (var_idx == nvars)
        continue;
      var2eq_cnt[var_idx]++;
      var2xored_eqs[var_idx] ^= eq_idx;
      eq2var_cnt[eq_idx]++;
    }
  }

  {
    var_states.resize(nvars);
    size_t idx = 0;
    for (auto &v : var_states) {
      v.idx = idx++;
      v.state = IDLE;
    }
  }

  eq_states.reserve(equations.size());
  for (size_t i = 0; i < equations.size(); i++) {
    eq_states.emplace_back(eq2var_cnt[i], SPARSE);

    if (eq2var_cnt[i] == 0) {
      zero_or_one_prio.push_front(i);
    } else if (eq2var_cnt[i] == 1) {
      zero_or_one_prio.push_back(i);
    }
  }

  debug_assert(eq_states.size() == equations.size(),
               "More states than equations!");
  debug_assert(var_states.size() == nvars, "More states than variables!");
}

template <>
typename mod_system<ModularSystemKind::MOD2>::value_type
mod_system<ModularSystemKind::MOD2>::eval_equation_for_var(
    const std::vector<mod_system<ModularSystemKind::MOD2>::value_type>
        &solution,
    size_t eq_idx, size_t var_idx) const {
  unsigned long acc = 0;
  bool present = false;
  for (auto idx : equations[eq_idx]) {
    if (idx == var_idx) {
      present = true;
      continue;
    }

    if (idx == nvars || solution[idx]) {
      acc ^= 1LU;
    }
  }

  return present ? acc : false;
}

template <>
mod_system<ModularSystemKind::MOD2>
mod_system<ModularSystemKind::MOD2>::build_dense_system(int active_vars) const {
  size_t next_idx = 0;
  std::vector<size_t> mapper(static_cast<size_t>(nvars + 1));
  for (size_t idx = 0; idx <= nvars; idx++) {
    if (idx == nvars || var_states[idx].state == ACTIVE) {
      mapper[idx] = next_idx++;
    } else {
      mapper[idx] = SIZE_MAX;
    }
  }

  std::vector<std::vector<size_t>> dense_equations{};
  for (size_t i = 0; i < nequations; i++) {
    const auto &eq = eq_states[i];
    if (eq.state != DENSE || ignored_eq[i])
      continue;

    std::vector<size_t> equation{};
    equation.reserve(static_cast<size_t>(active_vars) + 1);
    for (auto idx : equations[i]) {
      if (idx == nvars || var_states[idx].state == ACTIVE) {
        debug_assert(mapper[idx] != SIZE_MAX,
                     "Tried to add non active variable to dense system.");
        equation.emplace_back(mapper[idx]);
      }
    }
    dense_equations.emplace_back(equation);
  }
  return {modulo, static_cast<size_t>(active_vars), dense_equations};
}

#endif
